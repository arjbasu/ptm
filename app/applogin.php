<?php

	$status = "";
	$message = "";
	if(isset($_POST['username']) && isset($_POST['password']) && isset($_POST['logintype'])){
		include '../connect.php';
		$username =  mysql_real_escape_string($_POST['username']);
		$password = mysql_real_escape_string($_POST['password']);
		$logintype = $_POST['logintype'];
		
		if($username == "" || $password == "" || $logintype == ""){
			$status = "error";
			$message = "Please supply all the fields";
		}
		else{
			if($logintype == "admin" || $logintype == "client"){
				$prefix = $logintype;
				$table = "ptm_$logintype";
				$passhash = crypt($password,'$1$foreverdope12$');
				$query = "SELECT $prefix"."_id,$prefix"."_name FROM $table WHERE $prefix"."_username = ? AND $prefix"."_passhash = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($username,$passhash));
				if($stmt->rowCount() == 0){
					$status = "error";
					$message = "Improper Username Password Logintype combination suplied";
				}
				else{
					$temp = $stmt->fetch(PDO::FETCH_NUM);
					$userid = $temp[0];
					$fullname = $temp[1];
					$status = "success";
					$time = time();
					$key = md5("$userid$fullname$username$time");
					$query = "SELECT key_value FROM ptm_appkey WHERE key_userid = ? AND key_type = ?";
					$stmt = $pdo->prepare($query);
					$stmt->execute(array($userid,$logintype));
					if($stmt->rowCount() == 1){
						$temp = $stmt->fetch(PDO::FETCH_NUM);
						$status = "success";
						$key = $temp[0];
					}
					else{
						$query = "INSERT INTO ptm_appkey (key_userid,key_value,key_type) VALUES (?,?,?)";
						$stmt = $pdo->prepare($query);
						$stmt->execute(array($userid,$key,$logintype));
						if($stmt->rowCount() == 1){
							$status = "success";
						}
						else{
							$status = "error";
							$message = "Unable to update key";
						}
					}
				}
			}
			else{
				$status = "error";
				$message = "Improper Logintype supplied";
			}	
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	header('Access-Control-Allow-Origin: *');
	header('Content-type: application/json');
	if($status == "error"){
		require_once 'json_encode.php';
	}
	else{
		$response = array();
		$response['status'] = "success";
		$response['userid'] = $userid;
		$response['logintype'] = $logintype;
		$response['key'] = $key;
		$response['name'] = $fullname;
		echo json_encode($response);
	}
?>