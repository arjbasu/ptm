<?php
	require_once '../connect.php';
	require_once 'authentication_ajax_api.php';
	require_once 'check_admin.php';
	if(isset($_POST['job_id']) && isset($_POST['action'])){
		$jobid = $_POST['job_id'];
		$adminid = $userid;
		$action = $_POST['action'];
		if($action == "add"){
			if(isset($_POST['task_name']) && isset($_POST['eta'])){
				$taskname = $_POST['task_name'];
				$eta = $_POST['eta'];
				$query = "INSERT INTO ptm_tasks (task_name,task_jobid,task_adminid,task_eta) VALUES (?,?,?,?)";
				$stmt = $pdo->prepare($query);
				$result = $stmt->execute(array());
				if(!$result || $stmt->rowCount() == 0){
					$status = "error";
					$message = "Unable to add task";
				}
				else{
					$status = "success";
					$message = "Task successfully added";
				}
			}
			else{
				$status = "error";
				$message = "Improper paremeters passed";
			}
		}
		else if($action == "delete"){
			
		}
	} 
	else{
		$status = "error";
		$message = "Improper Parameters passed";
	}
	require_once 'json_encode.php';
?>
