<?php
	require_once '../connect.php';
	require_once 'authentication_ajax_api.php';
	require_once 'check_admin.php';
	
	if(isset($_POST['task_name']) && isset($_POST['task_jobid']) && isset($_POST['task_eta'])){
		$taskname = $_POST['task_name'];
		$jobid = $_POST['task_jobid'];
		$eta = $_POST['task_eta'];
		$query = "INSERT INTO ptm_tasks(task_name,task_jobid,task_eta,task_adminid) VALUES (?,?,?,?)";
		$stmt = $pdo->prepare($query);
		$result = $stmt->execute(array($taskname,$jobid,$eta,$_SESSION['ptm_userid']));
		if(!$result || $stmt->rowCount() != 1){
			$status = "error";
			$message = "Unable to add task";
		}
		else{
			$query = "SELECT task_id FROM ptm_tasks WHERE task_name = ? AND task_jobid = ? AND task_eta = ? AND task_adminid = ? ORDER BY task_timestamp DESC LIMIT 1";
			$stmt = $pdo->prepare($query);
			$result = $stmt->execute(array($taskname,$jobid,$eta,$_SESSION['ptm_userid']));
			if(!$result || $stmt->rowCount() == 0){
				$status = "error";
				$message = "Unable to get taskid";
			}
			else{
				$temp = $stmt->fetch(PDO::FETCH_ASSOC);
				$status = "success";
				$message = $temp['task_id'];
				
				$query = "SELECT job_completion,SUM(task_eta) AS total FROM ptm_jobs  INNER JOIN ptm_tasks ON task_jobid = job_id WHERE job_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($jobid));
				$resultvar = $stmt->fetch(PDO::FETCH_ASSOC);
				$totaldays = intval($resultvar['total']);
				
				$query = "SELECT job_completion,SUM(task_eta) AS total FROM ptm_jobs  INNER JOIN ptm_tasks ON task_jobid = job_id WHERE job_id = ? AND task_status = 'complete'";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($jobid));
				$resultvar = $stmt->fetch(PDO::FETCH_ASSOC);
				$newdays = intval($resultvar['total']);
				
				$newcompletion = round(($newdays/$totaldays)*100,2);
				$query = "UPDATE ptm_jobs SET job_completion = '$newcompletion' WHERE job_id = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($jobid)); 
				$message = $temp['task_id']."|$newcompletion|$taskname";
			}
			
			
		}
	} 
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	require_once 'json_encode.php';
?>