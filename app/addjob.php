<?php

	require_once '../connect.php';
	require_once 'authentication_ajax_api.php';
	require_once 'check_admin.php';
	
	if(isset($_POST['job_name']) && isset($_POST['job_clientid']) && isset($_POST['job_budget'])){
		$jobname = $_POST['job_name'];
		$clientid = $_POST['job_clientid'];
		$budget = str_replace(",", "", $_POST['job_budget']);
		$query = "INSERT INTO ptm_jobs (job_name,job_clientid,job_budget,job_adminid) VALUES (?,?,?,?)";
		$stmt = $pdo->prepare($query);
		$result = $stmt->execute(array($jobname,$clientid,$budget,$_SESSION['ptm_userid']));
		if(!$result || $stmt->rowCount() == 0){
			$status = "error";
			$message = "Unable to add new job";
		}
		else{
			$query = "SELECT admin_id FROM ptm_admin WHERE admin_id <> $userid";
			$result = mysql_query($query);
			while($temp = mysql_fetch_assoc($result)){
				$adminid = $temp['admin_id'];
				$query2 = "SELECT job_id FROM ptm_jobs WHERE job_name = ? AND job_clientid = ? AND  job_budget = ? AND job_adminid = ? ORDER BY job_timestamp DESC LIMIT 1";
				$stmt = $pdo->prepare($query2);
				$stmt->execute(array($jobname,$clientid,$budget,$userid));
				$tempres = $stmt->fetch(PDO::FETCH_ASSOC);
				$jobid = $tempres['job_id'];
// 				error_log("Hello:".$stmt->rowCount(),0);
				$query2 = "INSERT INTO ptm_adminnotifications (notification_adminid,notification_type,notification_jobid,notification_forid,notification_clientid) VALUES (?,?,?,?,?)";
				$stmt = $pdo->prepare($query2);
				$stmt->execute(array($userid,"addjob",$jobid,$adminid,$clientid));
				 
			}
			$status = "success";
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	} 
	require_once 'json_encode.php';
?>