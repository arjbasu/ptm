<?php
	require_once '../connect.php';
	require_once 'authentication_ajax_api.php';
	require_once 'check_admin.php';
	
	if(isset($_POST['job_id']) && isset($_POST['job_name']) && isset($_POST['job_budget'])){
		$jobid = $_POST['job_id'];
		$jobname = $_POST['job_name'];
		$budget = str_replace(",", "", $_POST['job_budget']);
		$query = "SELECT job_name FROM ptm_jobs WHERE job_id = ? ";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($jobid));
		if($stmt->rowCount() == 1){
			$query = "UPDATE ptm_jobs SET job_name = ? , job_budget = ? WHERE job_id = ?";
			$stmt = $pdo->prepare($query);
			$result = $stmt->execute(array($jobname,$budget,$jobid));
			if(!$result){
				$status = "error";
				$message = "unable to edit job";
			}
			else{
				$status = "success";
				$messaage = "Job Successfully edited";
			}
		}
		else{
			$status = "error";
			$message = "Job not found";
		}
		
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	require_once 'json_encode.php';
	 
?>