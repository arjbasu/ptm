<?php
	$status = "";
	$message = "";
	$logintype = "";
	$userid = 0;
	header('Access-Control-Allow-Origin: *');
	header('Content-type: application/json');
	function dead(){
		global $status;
		global $message;
		include 'json_encode.php';
		die();
	}
	if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH'])
			AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
		$status = "error";
		$message = "Only AJAX Requests will be processed";
		dead();
	}
	else if(!( $_POST['user_id'] && $_POST['app_id'])){
		$status = "error";
		$message = "Improper parameters passed";
		dead();
	}
	else{
		$userid = $_POST['user_id'];
		$appid = $_POST['app_id'];
	
		$query = "SELECT key_type FROM ptm_appkey WHERE key_value = ? AND key_userid = ?";
		$stmt = $pdo->prepare($query);
		$result = $stmt->execute(array($userid,$appid));
	
		if(!$result || $stmt->rowCount() == 0){
			$status = "authentication_error";
			$message = "Improper credentials supplied";
			dead();
		}
		else{
			$temp = $stmt->fetch(PDO::FETCH_NUM);
			$logintype = $temp[0];
		}
	} 
?>