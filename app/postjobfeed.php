<?php
	require_once '../connect.php';
	require_once 'authentication_ajax_api.php';
	require_once 'check_admin.php';
	if(isset($_POST['job_post']) && isset($_POST['job_id'])){
		$jobpost = $_POST['job_post'];
		$jobid = $_POST['job_id'];
		$clientid = $_POST['job_clientid'];
		$userid = $_SESSION['ptm_userid'];
		$query = "SELECT DATE_FORMAT(NOW(),'%M %D, %Y'),NOW()";
		$result = mysql_query($query);
		$time = mysql_fetch_array($result);
		$entrytime = $time[0];
		$time = $time[1];
		$query = "INSERT INTO ptm_clientfeed (feed_post,feed_jobid,feed_clientid,feed_timestamp,feed_adminid) VALUES (?,?,?,?,?)";
		$stmt = $pdo->prepare($query);
		$result = $stmt->execute(array($jobpost,$jobid,$clientid,$time,$_SESSION['ptm_userid']));
		if(!$result || $stmt->rowCount() != 1){
			$status = "error";
			$message = "Unable to post comment";
		}
		else{
			$query = "SELECT admin_id FROM ptm_admin WHERE admin_id <> $userid";
			$result = mysql_query($query);
			while($temp = mysql_fetch_assoc($result)){
				$adminid = $temp['admin_id'];
				$query2 = "INSERT INTO ptm_adminnotifications (notification_adminid,notification_type,notification_jobid,notification_forid) VALUES (?,?,?,?)";
				$stmt = $pdo->prepare($query2);
				$stmt->execute(array($userid,"feedupdate",$jobid,$adminid));
					
			}
			$query2 = "INSERT INTO ptm_clientnotifications (notification_adminid,notification_type,notification_jobid,notification_clientid) VALUES (?,?,?,?)";
			$stmt = $pdo->prepare($query2);
			$stmt->execute(array($userid,"feedupdate",$jobid,$clientid));
			$status = "success";
			$message = $entrytime;
		}
	} 
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	require_once 'json_encode.php';
?>