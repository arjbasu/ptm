<?php
	require_once '../connect.php';
	require_once 'authentication_ajax_api.php';
	require_once 'check_admin.php';
	if(isset($_POST['oldpassword']) && isset($_POST['newpassword']) && isset($_POST['confirmpassword'])){
		$oldpassword = $_POST['oldpassword'];
		$newpassword = $_POST['newpassword'];
		$confirmpassword = $_POST['confirmpassword'];
		
		$prefix = $logintype;
		
		$passhash = crypt($oldpassword,'$1$foreverdope12$');
		if($oldpassword == $newpassword){
			$status = "error";
			$message = "The new password is same as the old password";
		}
		else if($newpassword == $confirmpassword){
			$query = "SELECT $prefix"."_name FROM ptm_".$prefix." WHERE $prefix"."_id = ? AND $prefix"."_passhash = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($userid,$passhash));
			if($stmt->rowCount() != 1){
				$status = "error";
				$message = "Improper password provided";
			}
			else{
				$passhash = crypt($newpassword,'$1$foreverdope12$');
				$query = "UPDATE ptm_"."$prefix SET $prefix"."_passhash = ? WHERE $prefix"."_id = ?";
				$stmt = $pdo->prepare($query);
				$result = $stmt->execute(array($passhash,$userid));
				if(!$result || $stmt->rowCount() == 0){
					$status = "error";
					$message = "Unable to update password";
				}  
				else{
					$status = "success";
				}
			}
		}
		else{
			$status = "error";
			$message = "Passwords don't match";
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	include 'json_encode.php';
?>