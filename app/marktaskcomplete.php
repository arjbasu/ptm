<?php
	require_once '../connect.php';
	require_once 'authentication_ajax_api.php';
	require_once 'check_admin.php';

	if(isset($_POST['task_id']) && isset($_POST['task_action']) && isset($_POST['task_completion'])){
		$taskid = $_POST['task_id'];
		$action = $_POST['task_action'];
		$completion = floatval($_POST['task_completion']);
		if($action == "markcomplete"){
			$query = "SELECT task_status,task_jobid,task_eta,job_clientid FROM ptm_tasks INNER JOIN ptm_jobs ON task_jobid = job_id WHERE task_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($taskid));
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			$jobid = $temp['task_jobid'];
			$clientid = $temp['job_clientid'];
			$eta = $temp['task_eta'];
			if($temp['task_status'] == 'complete'){
				$status = "error";
				$message = "The task is already is marked as complete";
			}
			else{
				$query = "UPDATE ptm_tasks SET task_status = 'complete' WHERE task_id = ?";
				$stmt = $pdo->prepare($query);
				$result = $stmt->execute(array($taskid));
				if(!$result || $stmt->rowCount() != 1){
					$status = "error";
					$message = "Unable to mark status as read";
				}
				else{
					$query = "SELECT SUM(task_eta) AS totaldays FROM ptm_tasks WHERE task_jobid = '$jobid'";
					$result = mysql_query($query);
					if(!$result || mysql_num_rows($result) == 0){
						$status = "error";
						$message = "Unable to get job status";
					}
					else{
						$temp = mysql_fetch_assoc($result);
						$totaldays = intval($temp['totaldays']);
						$prevcompletion = $totaldays*$completion/100;
						$prevcompletion += $eta;
						$newcompletion = round(($prevcompletion/$totaldays)*100,2);
						
						
						
						$query = "SELECT task_id FROM ptm_tasks WHERE task_jobid = '$jobid' AND task_status = 'incomplete'";
						$result = mysql_query($query);
						if(mysql_num_rows($result) == 0){
							$newcompletion = 100.00;
						}
						$query = "UPDATE ptm_jobs SET job_completion = '$newcompletion' WHERE job_id = '$jobid'";
						$result = mysql_query($query);
						if(!$result || mysql_affected_rows() == 0){
							$status = "error";
							$message = "Unable to set percentage";
						}
						else{
							$query = "SELECT admin_id FROM ptm_admin WHERE admin_id <> $userid";
							$result = mysql_query($query);
							$update = "taskcomplete";
							if($newcompletion == 100.00){
								$update = "jobcomplete";
							}
							while($temp = mysql_fetch_assoc($result)){
								$adminid = $temp['admin_id'];
								$query2 = "INSERT INTO ptm_adminnotifications (notification_clientid,notification_adminid,notification_type,notification_jobid,notification_taskid,notification_forid,notification_completion) VALUES (?,?,?,?,?,?,?)";
								$stmt = $pdo->prepare($query2);
								$stmt->execute(array($clientid,$userid,$update,$jobid,$taskid,$adminid,$newcompletion));
									
							}
							$query2 = "INSERT INTO ptm_clientnotifications (notification_adminid,notification_type,notification_jobid,notification_taskid,notification_clientid,notification_completion) VALUES (?,?,?,?,?,?)";
							$stmt = $pdo->prepare($query2);
							error_log("client_id:$clientid",0);
							$stmt->execute(array($userid,$update,$jobid,$taskid,$clientid,$newcompletion));
							$status = "success";
							$message = $newcompletion;
						}
					}
				}
			}
		}
		else if($action == "markincomplete"){
			$query = "SELECT task_status,task_jobid,task_eta FROM ptm_tasks WHERE task_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($taskid));
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			$jobid = $temp['task_jobid'];
			$eta = $temp['task_eta'];
			if($temp['task_status'] == 'incomplete'){
				$status = "error";
				$message = "The task is already is marked as incomplete";
			}
			else{
				$query = "UPDATE ptm_tasks SET task_status = 'incomplete' WHERE task_id = ?";
				$stmt = $pdo->prepare($query);
				$result = $stmt->execute(array($taskid));
				if(!$result || $stmt->rowCount() != 1){
					$status = "error";
					$message = "Unable to mark status as read";
				}
				else{
					$query = "SELECT SUM(task_eta) AS totaldays FROM ptm_tasks WHERE task_jobid = '$jobid'";
					$result = mysql_query($query);
					if(!$result || mysql_num_rows($result) == 0){
						$status = "error";
						$message = "Unable to get job status";
					}
					else{
						$temp = mysql_fetch_assoc($result);
						$totaldays = intval($temp['totaldays']);
						$prevcompletion = $totaldays*$completion/100;
						$prevcompletion -= $eta;
						$newcompletion = round(($prevcompletion/$totaldays)*100,2);
						
						$query = "SELECT task_id FROM ptm_tasks WHERE task_jobid = '$jobid' AND task_status = 'complete'";
						$result = mysql_query($query);
						if(mysql_num_rows($result) == 0){
							$newcompletion = 0.00;
						}
						
						$query = "UPDATE ptm_jobs SET job_completion = '$newcompletion' WHERE job_id = '$jobid'";
						$result = mysql_query($query);
						if(!$result || mysql_affected_rows() == 0){
							$status = "error";
							$message = "Unable to set percentage";
						}
						else{
							
							
							$status = "success";
							$message = $newcompletion;
						}
					}
				}
			}
		}
		else{
			$status = "error";
			$message = "Improper action passed";
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	require_once 'json_encode.php';
?>