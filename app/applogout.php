<?php
	$status = "";
	$message = "";
	if(isset($_POST['app_id']) && isset($_POST['user_id']) && isset ($_POST['login_type'])){
		require_once '../connect.php';
		$appid = $_POST['app_id'];
		$userid = $_POST['user_id'];
		$logintype = $_POST['login_type'];
		$query = "SELECT * FROM ptm_appkey WHERE key_value = ? AND key_userid = ? AND key_type = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($appid,$userid,$logintype));
		if($stmt->rowCount() == 0){
			$status = "error";
			$message = "No Login found";
		}
		else{
			$query = "DELETE FROM ptm_appkey WHERE key_value = ? AND key_userid = ? AND key_type = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($appid,$userid,$logintype));
			if($stmt->rowCount() == 1){
				$status = "success";
			}
			else{
				$status = "error";
				$message = "Unable to logout. Please try again later";
			}
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	header('Access-Control-Allow-Origin: *');
	header('Content-type: application/json');
	require_once 'json_encode.php';
?>