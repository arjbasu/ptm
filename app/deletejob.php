<?php
	require_once '../connect.php';
	require_once 'authentication_ajax_api.php';
	require_once 'check_admin.php';
	if(isset($_POST['job_id'])){
		$jobid = $_POST['job_id'];
		$query = "DELETE FROM ptm_jobs WHERE job_id = ?";
		$stmt = $pdo->prepare($query);
		$result = $stmt->execute(array($jobid));
		if(!$result || $stmt->rowCount() == 0){
			$status = "error";
			$message = "Unable to delete job";
		}
		else{
			$query = "DELETE FROM ptm_tasks WHERE task_jobid = ?";
			$stmt = $pdo->prepare($query);
			$result = $stmt->execute(array($jobid));
			if(!$result){
				$status = "error";
				$message = "Unable to delete related tasks";
			}
			else{
				$status = "success";
			}
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	} 
	require_once 'json_encode.php';
?>