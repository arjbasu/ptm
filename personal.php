<?php
	session_start();
	include 'check_authorization.php';
	include 'connect.php';
	if(isset($_POST['username']) && $_POST['fullname']){
		$username = $_POST['username'];
		$fullname = $_POST['fullname'];
		$userid = $_SESSION['ptm_userid'];
		if($_SESSION['ptm_logintype'] == "admin"){
			$prefix = "admin";
		}
		else if($_SESSION['ptm_logintype'] == "client"){
			$prefix = "client";
		}
		$query = "SELECT $prefix"."_username, $prefix"."_name FROM ptm_$prefix WHERE $prefix"."_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($userid));
		$temp = $stmt->fetch(PDO::FETCH_NUM);
		$flag = true;
		if($username != $temp[0]){
			$query = "SELECT $prefix"."_username, $prefix"."_name FROM ptm_$prefix WHERE $prefix"."_username = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($username));
			if($stmt->rowCount() != 0){
				$flag = false;
			}	
		}
		if($flag == true){
// 			error_log("Everything's fine");
			$query = "UPDATE ptm_"."$prefix SET $prefix"."_name = ?, $prefix"."_username = ? WHERE $prefix"."_id = ?";
			error_log("query:".$query);
			$stmt = $pdo->prepare($query);
			$result = $stmt->execute(array($fullname,$username,$userid));
		}
	} 
	header("Location:settings.php");
?>