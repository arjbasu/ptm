<?php
session_start();
include 'check_authorization.php';
include 'twiginit.php';
include 'connect.php';
include 'generatenavdata.php';

function adminjobs(){
	global $data;
	global $twig;
	global $pdo;
	$userid = $_SESSION['ptm_userid'];
	$data['name'] = $_SESSION['ptm_username'];
	$query = "SELECT feed_post,admin_name,feed_jobid,DATE_FORMAT(feed_timestamp,'%b %D, %Y') AS feed_timestamp FROM ptm_clientfeed INNER JOIN ptm_admin
	ON feed_adminid = admin_id";
	$feed = array();
	$result = mysql_query($query);
	if(!$result){
		die("unable to get feeds");
	}
	else{
		while($temp = mysql_fetch_assoc($result)){
			$temp['feed_post'] = stripslashes($temp['feed_post']);
			$jobid = $temp['feed_jobid'];
			$temp['feed_date'] = $temp['feed_timestamp'];
			if(isset($feed[$jobid])){
				array_push($feed[$jobid], $temp);
			}
			else{
				$feed[$jobid] = array();
				array_push($feed[$jobid], $temp);
			}
		}
	}
	$query = "SELECT task_id,task_name,task_jobid,task_status,task_eta FROM ptm_tasks";
	$result = mysql_query($query);
	$tasks = array();
	if(!$result){
		die("unable to get tasks");
	}
	else{
		while($temp = mysql_fetch_assoc($result)){
			$jobid = $temp['task_jobid'];
			if(isset($tasks[$jobid])){
				array_push($tasks[$jobid], $temp);
			}
			else{
				$tasks[$jobid] = array();
				array_push($tasks[$jobid], $temp);
			}
			
		}
	}
	$query = "SELECT job_id,job_name,job_completion,job_clientid,client_name,job_budget,client_organization AS org,FORMAT(job_budget,2) AS job_budget
	,DATE_FORMAT(adddate(job_timestamp,INTERVAL SUM(task_eta) day ),'%M %D, %Y') AS eta FROM ptm_jobs INNER JOIN ptm_client ON
	 client_id = job_clientid LEFT JOIN ptm_tasks ON task_jobid = job_id GROUP BY job_id ORDER BY job_timestamp DESC ";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to fetch jobs");
	}
	else{
		$jobs = array();
		while($temp = mysql_fetch_assoc($result)){
			$jobid = $temp['job_id'];
			if(isset($tasks[$jobid])){
				$temp['tasks'] = $tasks[$jobid];
			}
			if(isset($feed[$jobid])){
				$temp['feed'] = $feed[$jobid];
			}
			array_push($jobs,$temp);
		}
		if(count($jobs) != 0){
			$data['jobs'] = $jobs;
		}
	}
	$query = "SELECT client_id,client_name FROM ptm_client";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to fetch clients");
	}
	else{
		$clients = array();
		while($temp = mysql_fetch_assoc($result)){
			array_push($clients,$temp);
		}
		$data['clients'] = $clients;
	}
	//print_r($data);
	echo $twig->render("admin-jobs.twig",$data);
}
function clientjobs(){
	global $twig;
	global $pdo;
	global $data;
	$data['name'] = $_SESSION['ptm_username'];
	$query = "SELECT job_name,client_name,client_organization,client_description,";
	echo $twig->render("client-jobs.twig",$data);
}


if($_SESSION['ptm_logintype'] == "admin"){
	adminjobs();
}
else if($_SESSION['ptm_logintype'] == "client"){
	clientjobs();
}
else{
	header("Location:index.php");
}


	
	
?>