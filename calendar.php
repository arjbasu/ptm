<?php
	session_start();
	include 'check_authorization.php';
	include 'twiginit.php';
	include 'connect.php';
	include 'generatenavdata.php';
	
	$data['name'] = $_SESSION['ptm_username'];
	echo $twig->render("calendar.twig",$data);

?>