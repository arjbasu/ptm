<?php
	session_start();
	$data = array();
	include 'check_authorization.php';
	include 'twiginit.php';
	include 'connect.php';
	include 'generatenavdata.php';
	
	function adminpendingjobs(){
		global $pdo;
		global $twig;
		global $data;
		$data['name'] = $_SESSION['ptm_username'];
		$query = "SELECT * FROM ptm_pendingjobs";
		$result = mysql_query($query);
		if(!$result){
			die("Unable to get pending jobs");
		}
		else if(mysql_num_rows($result) != 0){
			$jobs = array();
			while($temp = mysql_fetch_assoc($result)){
				array_push($jobs,$temp);
			}
			$data['jobs'] = $jobs;
		}
		echo $twig->render("pending.twig",$data);
	}
	
	if($_SESSION['ptm_logintype'] == "admin"){
		adminpendingjobs();
	}
	else{
		header("Location:index.php");
	}
	
	
?>