<?php
	session_start();
	if(isset($_SESSION['ptm_username']) && isset($_SESSION['ptm_userid']) && isset($_SESSION['ptm_logintype']) && isset($_SESSION['ptm_logged'])){
		header("Location:dashboard.php");
	} 
	else{
		include 'twiginit.php';
		echo $twig->render("login.twig");	
	}
?>