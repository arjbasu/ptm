<?php
	if($_SESSION['ptm_logintype'] == "admin"){
		$userid = $_SESSION['ptm_userid'];
		$query = "SELECT admin_name as name , admin_id as id FROM ptm_admin WHERE admin_id <> $userid";
		$result = mysql_query($query);
		$admins = array();
		while($temp = mysql_fetch_assoc($result)){
			array_push($admins,$temp);
		}
		$data['admins'] = $admins;
		$query = "SELECT job_name,job_completion AS percent FROM ptm_jobs WHERE job_completion < 100 ORDER BY job_completion DESC";
		$result = mysql_query($query);
		$jobs = array();
		if(!$result){
			die("Unable to fetch jobs");
		}
		
		else{
			while($temp = mysql_fetch_assoc($result)){
				array_push($jobs,$temp);
			}
		}
		
		$userid = $_SESSION['ptm_userid'];
		$query = "SELECT notification_adminid ,admin_name AS name,notification_type AS type,job_name,notification_completion AS job_completion,client_name,client_organization AS org,task_name FROM ptm_adminnotifications
		INNER JOIN ptm_admin ON admin_id = notification_adminid LEFT JOIN ptm_jobs ON notification_jobid = job_id
		LEFT JOIN ptm_tasks ON notification_taskid = task_id LEFT JOIN ptm_client ON notification_clientid = client_id
		WHERE notification_forid = '$userid' AND notification_status = 'unread' ORDER BY notification_id DESC ";
		//error_log("query:".$query,0);
		$result = mysql_query($query);
		$notifications = array();
		if(!$result){
			die("unable to fetch notifications");
		}
		else{
			while($temp = mysql_fetch_assoc($result)){
				$temparray = array();
				if($temp['type'] == "addclient"){
					$temparray['notice'] = $temp['name']." added a new client: ".$temp['client_name'];
					$temparray['href'] = "clients.php";
				}
				else if($temp['type'] == "addjob"){
					$temparray['notice'] = $temp['name']." added a new job for ".$temp['org'];
					$temparray['href'] = "jobs.php";
				}
				else if($temp['type'] == "addtask"){
					$temparray['notice'] = $temp['name']." added a new task '".$temp['task_name']."' to the job ".$temp['job_name'];
					$temparray['href'] = "jobs.php";
				}
				else if($temp['type'] == "taskcomplete"){
					$temparray['notice'] = $temp['name']." completed the task '".$temp['task_name']."' for the job ".$temp['job_name'].". It is now ".$temp['job_completion']."% complete";
					$temparray['href'] = "jobs.php";
				}
				else if($temp['type'] == 'jobcomplete'){
					$temparray['notice'] = $temp['name']." completed the job '". $temp['job_name'] ."'  for ".$temp['org'];
					$temparray['href'] = "jobs.php";
				}
				else if($temp['type'] == "feedupdate"){
					$temparray['notice'] = $temp['name']." gave ".$temp['job_name']." an update";
					$temparray['href'] = "jobs.php";
				}
				else if($temp['type'] == 'calendarupdate'){
					$temparray['notice'] = $temp['name']." update the calendar";
					$temparray['href'] = "calendar.php";
				}
				else if($temp['type'] == 'revenueupdate'){
					$temparray['notice'] = $temp['name']." updated the company revenue";
					$temparray['href'] = "settings.php";
				}
				else if($temp['type'] == 'excelupdate'){
					$temparray['notice'] = $temp['name']." edited the pending jobs";
					$temparray['href'] = "dashboard.php";
				}
				else if($temp['type'] == "todoupdate"){
					$temparray['notice'] = $temp['name']." added an item to your To-do list.";
					$temparray['href'] = "todo.php";
				}
					
				array_push($notifications,$temparray);
			}
		}
		$data['admin'] = true;
		$data['navnotifications'] = $notifications;
		$data['navjobs'] = $jobs;
	}
	else if($_SESSION['ptm_logintype'] == "client"){
		$userid = $_SESSION['ptm_userid'];
		$query = "SELECT notification_adminid ,admin_name AS name,notification_type AS type,job_name,notification_completion AS job_completion,task_name FROM ptm_clientnotifications
		INNER JOIN ptm_admin ON admin_id = notification_adminid LEFT JOIN ptm_jobs ON notification_jobid = job_id
		LEFT JOIN ptm_tasks ON notification_taskid = task_id 
		WHERE notification_clientid = '$userid' AND notification_status = 'unread' ORDER BY notification_id DESC";
		//error_log("query:".$query,0);
		$result = mysql_query($query);
		$growlarray = array();
		$notifications = array();
		if(!$result){
			die("unable to fetch notifications");
		}
		else{
			
			 
			while($temp = mysql_fetch_assoc($result)){
				$temparray = array();
				$temparray['notice'] = $temp['name'];
				if($temp['type'] == "taskcomplete"){
					$temparray['notice'] .= " completed the task '".$temp['task_name']."' for the job ".$temp['job_name'].". It is now ".$temp['job_completion']."% complete";
					$temparray['href'] = "#";
				}
				else if($temp['type'] == "feedupdate"){
					$temparray['notice'] .= " gave ".$temp['job_name']." an update";
					$temparray['href'] = "#";
				}
				else if($temp['type'] == 'jobcomplete'){
					array_push($growlarray, $temp['job_name']);
					$temparray['notice'] .= " completed the job '".$temp['job_name']."'";
					$temparray['href'] = "#";
				}
				array_push($notifications, $temparray);
			}		
		}
		error_log("notifications:".count($notifications),0);
		$data['navnotifications'] = $notifications;
		$data['growlnotifications'] = $growlarray;
	}
?>