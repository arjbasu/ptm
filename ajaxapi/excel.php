<?php
	session_start();
	$status = "";
	$message = "";
	include 'authentication_ajax_api.php';
	include '../connect.php';
	if(isset($_POST['client_name']) && isset($_POST['organization']) && isset($_POST['email']) &&
	isset($_POST['phone']) && isset($_POST['job_title']) && isset($_POST['date']) 
			&& isset($_POST['status'])){
		$query = "BEGIN";
		$stmt = $pdo->prepare($query);
		$result = $stmt->execute();
		$query = "TRUNCATE TABLE ptm_adminexcel";
		$stmt = $pdo->prepare($query);
		$result = $stmt->execute();
		if(!$result){
			$status = "error";
			$message = "Unable to save excel. Please try again later";
		}
		else{
			$excel = array();
			$excel['phone'] = $_POST['phone'];
			$excel['org'] = $_POST['organization'];
			$excel['email'] = $_POST['email'];
			$excel['job'] = $_POST['job_title'];
			$excel['status'] = $_POST['status'];
			$excel['client'] = $_POST['client_name'];
			$excel['date'] = $_POST['date'];
			$len = count($excel['phone']);
			error_log("length:".$len,0);
			$query = "INSERT INTO ptm_adminexcel (excel_jobname,excel_clientname,excel_organization,excel_email,excel_phone,excel_date,excel_status) VALUES ";
			$insertquery = array();
			$insertdata = array();
			for($i = 0; $i < $len; $i++){
				$insertquery[] = " (?,?,?,?,?,?,?)";
				$insertdata[] = $excel['job'][$i];
				$insertdata[] = $excel['client'][$i];
				$insertdata[] = $excel['org'][$i];
				$insertdata[] = $excel['email'][$i];
				$insertdata[] = $excel['phone'][$i];
				$insertdata[] = $excel['date'][$i];
				$insertdata[] = $excel['status'][$i];
			}
			if(!empty($insertquery)){
				$query.= implode(', ',$insertquery);
				$stmt = $pdo->prepare($query);
				error_log("query:".$query."|");
				$stmt->execute($insertdata);
				if($stmt->rowCount() <1){
					$stmt = $pdo->prepare("ROLLBACK");
					$stmt->execute();
					$status = "error";
					$message = "Unable to insert into database";
				}
				else{
					$userid = $_SESSION['ptm_userid'];
					$query = "SELECT admin_id FROM ptm_admin WHERE admin_id <> $userid";
					$result = mysql_query($query);
					$query = "INSERT INTO ptm_adminnotifications (notification_adminid,notification_type,notification_forid) VALUES (?,?,?)";
					$stmt = $pdo->prepare($query);
					
					while($temp = mysql_fetch_assoc($result)){
						$adminid = $temp['admin_id'];
						$notresult = $stmt->execute(array($userid,"excelupdate",$adminid));
						if(!$notresult){
							$status = "error";
							$message = "Unable to generate notifications";
							$stmt = $pdo->prepare("ROLLBACK");
							$stmt->execute();
							break;
						}
					}
					$stmt = $pdo->prepare("COMMIT");
					$stmt->execute();
					$status = "success";
					$message = "Requests successfully sent";
				}
			}
			else{
				$status = "error";
				$message = "Empty Excel data";
			}
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}  
	include 'json_encode.php';
?>