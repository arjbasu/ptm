<?php
	session_start();
	include '../connect.php';
	if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH'])
			AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
		$status = "error";
		$message = "Only AJAX Requests will be processed";
	}
	if(isset($_POST['username']) && isset($_POST['password']) && isset($_POST['logintype'])){
		$username =  mysql_real_escape_string($_POST['username']);
		$password = mysql_real_escape_string($_POST['password']);
		$logintype = $_POST['logintype'];
		
		if(isset($_POST['rememberme'])){
			$remember = $_POST['rememberme'];
		}
			
		else{
			$remember = "";
		}
		
		if($logintype == "admin"){
			$prefix = "admin";
		}
		else if($logintype == "client"){
			$prefix = "client";
		}
		else{
			$status = "error";
			$message = "Improper parameters passed";
		}
		
		if(isset($prefix)){
			$passhash = crypt($password,'$1$foreverdope12$');
			$query = "SELECT ".$prefix."_name,".$prefix."_id FROM ptm_$prefix WHERE ".$prefix."_passhash = '$passhash' AND ".$prefix."_username = '$username'";
			$result = mysql_query($query);
			if(!$result || mysql_num_rows($result) == 0){
				$status = "error";
				$message = "Unable to login. Improper username or password";
			}
			else{
				$temp = mysql_fetch_assoc($result);
				$index = $prefix."_name";
				$username = $temp[$index];
				$index = $prefix."_id";
				$userid = $temp[$index];
				if($remember == "remember"){
					setcookie("user",$username,time()+14*24*60*60,"/");
					setcookie("user_id",$userid,time()+14*24*60*60,"/");
					setcookie("logintype",$prefix,time() + 14*24*60*60,"/");
				}
				$_SESSION['ptm_logged'] = true;
				$_SESSION['ptm_username'] = $username;
				$_SESSION['ptm_userid'] = $userid;
				if($prefix == "client"){
					$_SESSION['ptm_logintype'] = 'client';
				}
				else{
					$_SESSION['ptm_logintype'] = 'admin';
				}
				$status = "success";
				$message = "Successfully logged in";
			}
			
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	include 'json_encode.php';
?>