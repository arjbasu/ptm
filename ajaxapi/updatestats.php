<?php
	session_start();
	require_once 'authentication_ajax_api.php';
	require_once '../connect.php';
	if(isset($_POST['completed_jobs']) && isset($_POST['working_jobs']) && isset($_POST['pending_jobs'])
			&& isset($_POST['coffee_cups'])){
		$completed = $_POST['completed_jobs'];
		$working = $_POST['working_jobs'];
		$pending = $_POST['pending_jobs'];
		$coffee = $_POST['coffee_cups'];
		$rangear = range(0,100);
		if(in_array($completed, $rangear) && in_array($pending, $rangear) && in_array($working,$rangear) && in_array($coffee,$rangear)){
			$query = "UPDATE ptm_stats SET completed_jobs = ?, working_jobs = ?,pending_jobs = ?, coffee_cups = ?";
			$stmt = $pdo->prepare($query);
			$result = $stmt->execute(array($completed,$working,$pending,$coffee));
			if(!$result){
				$status = "error";
				$message = "Unable to update values";
			}
			else{
				$status = "success";
				$message = "Stats updated successfully";
			}
		}
		else{
			$status = "error";
			$message = "Out of range! Please use values between 0 and 100 only";
		}
	} 
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	
	include 'json_encode.php';
?>