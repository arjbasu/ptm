<?php
	session_start();
	$status = "";
	$message = "";
	require_once 'authentication_ajax_api.php';
	require_once '../connect.php';
	if(isset($_POST['task_id']) && isset($_POST['action'])){
		$taskid = $_POST['task_id'];
		$action = $_POST['action'];
		
		if($action == "delete"){
			$query = "SELECT task_eta,task_status,job_completion,job_id,job_clientid FROM ptm_tasks INNER JOIN ptm_jobs ON job_id = task_jobid WHERE task_id = ?";
			$stmt = $pdo->prepare($query);
			$result = $stmt->execute(array($taskid));
			if(!$result || $stmt->rowCount() == 0){
				$status = "error";
				$message = "Unable to get task";
			}
			else{
				$temp = $stmt->fetch(PDO::FETCH_ASSOC);
				$eta = $temp['task_eta'];
				$completion = floatval($temp['job_completion']);
				$jobid = $temp['job_id'];
				$clientid = $temp['job_clientid'];
				$task_status = $temp['task_status'];
				if($completion != 100.00){
					$query = "SELECT SUM(task_eta) FROM ptm_tasks WHERE task_jobid = '$jobid'";
					$result = mysql_query($query);
					$temp = mysql_fetch_row($result);
					$totaldays = intval($temp[0]);
					
					$query = "SELECT SUM(task_eta) FROM ptm_tasks WHERE task_jobid = '$jobid' AND task_status = 'complete'";
					$result = mysql_query($query);
					$temp = mysql_fetch_row($result);
					$prevcompletion = intval($temp[0]);
					if($task_status == 'complete'){
						$prevcompletion-= $eta;
					}
					
					$totaldays-= $eta;
					
					
					$newcompletion = round(($prevcompletion/$totaldays)*100,2);
					
				}
				else{
					$newcompletion = $completion;
				}
				
				$stmt = $pdo->prepare("BEGIN");
				$stmt->execute();
				$query = "UPDATE ptm_jobs SET job_completion = $newcompletion WHERE job_id = $jobid";
				$stmt = $pdo->prepare($query);
				$result = $stmt->execute();
				if(!$result ){
					$status = "error";
					$message= "Unable to update job_completion";
					$stmt = $pdo->prepare("ROLLBACK");
					$stmt->execute();
				}
				else{
					$query = "DELETE FROM ptm_tasks WHERE task_id= ?";
					$stmt = $pdo->prepare($query);
					$result = $stmt->execute(array($taskid));
					if(!$result || $stmt->rowCount() == 0){
						$status = "error";
						$message = "Unable to delete task";
						$stmt = $pdo->prepare("ROLLBACK");
						$stmt->execute();
					}
					else{
						$stmt = $pdo->prepare("COMMIT");
						$stmt->execute();
						$status = "success";
						$message = $newcompletion;
						
						if($newcompletion == 100.00){
							$update = "jobcomplete";
							$userid = $_SESSION['ptm_userid'];
							$query = "SELECT admin_id FROM ptm_admin WHERE admin_id <> $userid";
							$result = mysql_query($query);
							
							while($temp = mysql_fetch_assoc($result)){
								$adminid = $temp['admin_id'];
								$query2 = "INSERT INTO ptm_adminnotifications (notification_clientid,notification_adminid,notification_type,notification_jobid,notification_taskid,notification_forid,notification_completion) VALUES (?,?,?,?,?,?,?)";
								$stmt = $pdo->prepare($query2);
								$stmt->execute(array($clientid,$userid,$update,$jobid,$taskid,$adminid,$newcompletion));
							
							}
							$query2 = "INSERT INTO ptm_clientnotifications (notification_adminid,notification_type,notification_jobid,notification_taskid,notification_clientid,notification_completion) VALUES (?,?,?,?,?,?)";
							$stmt = $pdo->prepare($query2);
							error_log("client_id:$clientid",0);
							$stmt->execute(array($userid,$update,$jobid,$taskid,$clientid,$newcompletion));
						}
						
						
					}
				}
					
			}
			
		}
		
		else if($action == "edit"){
			if(isset($_POST['task_name'])  && isset ($_POST['task_eta'])){
				$taskname = $_POST['task_name'];
				$neweta = intval($_POST['task_eta']);
				$query = "SELECT task_eta,task_status,job_completion,job_id FROM ptm_tasks INNER JOIN ptm_jobs ON job_id = task_jobid WHERE task_id = ?";
				$stmt = $pdo->prepare($query);
				$result = $stmt->execute(array($taskid));
				if(!$result || $stmt->rowCount() == 0){
					$status = "error";
					$message = "Unable to get task";
				}
				else{
					$temp = $stmt->fetch(PDO::FETCH_ASSOC);
					$eta = $temp['task_eta'];
					$completion = floatval($temp['job_completion']);
					$jobid = $temp['job_id'];
					$task_status = $temp['task_status'];
					if($eta == $neweta){
						$query = "UPDATE ptm_tasks SET task_name = ? WHERE task_id = ?";
						$stmt = $pdo->prepare($query);
						$result = $stmt->execute(array($taskname,$taskid));
						if(!$result){
							$status = "error";
							$message = "Unable to delete task";
						}
						else{
							$status = "success";
							$message = $completion;
						}
					}
					else{
						if($completion != 100.00){
							$query = "SELECT SUM(task_eta) FROM ptm_tasks WHERE task_jobid = '$jobid'";
							$result = mysql_query($query);
							$temp = mysql_fetch_row($result);
							$totaldays = intval($temp[0]);
							
							
							$query = "SELECT SUM(task_eta) FROM ptm_tasks WHERE task_jobid = '$jobid' AND task_status = 'complete'";
							$result = mysql_query($query);
							$temp = mysql_fetch_row($result);
							$prevcompletion = intval($temp[0]);
							
							if($task_status == 'complete'){
								$prevcompletion += ($neweta-$eta);
								$totaldays += ($neweta-$eta);
							}
							else{
								$totaldays += ($neweta-$eta);
							}
							$newcompletion = round(($prevcompletion/$totaldays)*100,2);
								
						}
						else{
							$newcompletion = $completion;
						}
						
						$stmt = $pdo->prepare("BEGIN");
						$stmt->execute();
						$query = "UPDATE ptm_jobs SET job_completion = $newcompletion WHERE job_id = $jobid";
						$stmt = $pdo->prepare($query);
						$result = $stmt->execute();
						if(!$result ){
							$status = "error";
							$message= "Unable to update job_completion";
							$stmt = $pdo->prepare("ROLLBACK");
							$stmt->execute();
						}
						else{
							$query = "UPDATE ptm_tasks SET task_name = ?,task_eta = ? WHERE task_id = ?";
							$stmt = $pdo->prepare($query);
							$result = $stmt->execute(array($taskname,$neweta,$taskid));
							if(!$result){
								$status = "error";
								$message = "Unable to delete task";
								$stmt = $pdo->prepare("ROLLBACK");
								$stmt->execute();
							}
							else{
								$status = "success";
								$stmt = $pdo->prepare("COMMIT");
								$stmt->execute();
								$message = $newcompletion;
							}
						}
					}
				}
			}
			else{
				$status = "error";
				$message = "Improper parameters passed";
			}
		}
		else{
			$status = "error";
			$message = "Improper action parameter passed";
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	include 'json_encode.php';
?>