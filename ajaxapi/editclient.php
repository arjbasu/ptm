<?php
	session_start();
	include 'authentication_ajax_api.php';
	$status = "";
	$message = "";
	if(isset($_POST['client_id']) && $_POST['action']){
		include '../connect.php';
		$clientid = $_POST['client_id'];
		$action = $_POST['action'];
		if($action == "delete"){
			$query = "DELETE FROM ptm_client WHERE client_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($clientid));
			if($stmt->rowCount() == 1){
				$status = "success";
			}
			else{
				$status = "error";
				$message = "Unable to delete client";
			}
		}
		else if($action == "edit"){
			if(isset($_POST['client_newpass']) && isset($_POST['client_oldpass']) && isset($_POST['client_confirmpass'])){
				if($_POST['client_newpass'] == $_POST['client_confirmpass']){
						
					$oldpasshash = crypt($_POST['client_oldpass'],'$1$foreverdope12$');
					$newpasshash = crypt($_POST['client_newpass'],'$1$foreverdope12$');
					$query = "SELECT client_id FROM ptm_client WHERE client_id = ? AND client_passhash = ?";
					$stmt = $pdo->prepare($query);
					$stmt->execute(array($clientid,$oldpasshash));
					if($stmt->rowCount() == 1){
						$query = "UPDATE ptm_client SET ptm_passhash = ? WHERE client_id = ?";
						$stmt = $pdo->prepare($query);
						$stmt->execute(array($newpasshash,$clientid));
						if($stmt->rowCount() == 1){
							$status = "success";
							$message = "";
						}
					}
					else{
						$status = "error";
						$message = "Wrong password provided";
					}
				}
				else{
					$status = "error";
					$message = "Passwords do not match";
				}
			}
			else{
				$status = "error";
				$message = "Improper parameters passed";
			}
		}
	} 
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	include 'json_encode.php';
?>