<?php
	session_start();
	header('Content-type: application/json');
	$status = "";
	$message = "";
	require_once 'authentication_ajax_api.php';
	require_once '../connect.php';
	if(isset($_POST['pjob_id']) && isset($_POST['action'])){
		$pjob_id = $_POST['pjob_id'];
		$action = $_POST['action'];
		
		if($action == "create"){
				if(isset($_POST['pjob_name']) && isset($_POST['pjob_clientname']) && isset($_POST['pjob_organization'])
						&& $_POST['pjob_email'] && isset($_POST['pjob_phone']) && isset($_POST['pjob_status']) && isset($_POST['pjob_description'])){
					
					$pjob_name = $_POST['pjob_name'];
					$pjob_clientname = $_POST['pjob_clientname'];
					$pjob_organization = $_POST['pjob_organization'];
					$pjob_status = $_POST['pjob_status'];
					$pjob_phone = $_POST['pjob_phone'];
					$pjob_email = $_POST['pjob_email'];
					$pjob_description = $_POST['pjob_description'];
					
					$query = "INSERT INTO ptm_pendingjobs (pjob_name, pjob_clientname, pjob_companyname,pjob_phone,pjob_email,pjob_status,pjob_description) VALUES (?,?,?,?,?,?,?)";
					$stmt = $pdo->prepare($query);
					$result = $stmt->execute(array($pjob_name,$pjob_clientname,$pjob_organization,$pjob_phone,$pjob_email,$pjob_status,$pjob_description));
					if(!$result || $stmt->rowCount() == 0){
						$status = "error";
						$message = "Unable to insert into table";
					}
					else{
						$status = "success";
						$message = "Job successfully added";
					}
					
				}
				else{
					$status = "error";
					$message = "Improper parameters passed for job creation";
				}
		}
		else if($action == "update"){
			
			$query = "SELECT * FROM ptm_pendingjobs WHERE pjob_id = ?";
			$stmt = $pdo->prepare($query);
			$result = $stmt->execute(array($pjob_id));
			if(!$result || $stmt->rowCount() == 0){
				$status = "error";
				$message = "Job not found";
			}
			else{
				if(isset($_POST['pjob_name']) && isset($_POST['pjob_clientname']) && isset($_POST['pjob_organization'])
						&& $_POST['pjob_email'] && isset($_POST['pjob_phone']) && isset($_POST['pjob_status']) && isset($_POST['pjob_description'])){
						
					$pjob_name = $_POST['pjob_name'];
					$pjob_clientname = $_POST['pjob_clientname'];
					$pjob_organization = $_POST['pjob_organization'];
					$pjob_status = $_POST['pjob_status'];
					$pjob_phone = $_POST['pjob_phone'];
					$pjob_email = $_POST['pjob_email'];
					$pjob_description = $_POST['pjob_description'];
						
					$query = "UPDATE ptm_pendingjobs SET pjob_name = ?, pjob_clientname = ?, pjob_companyname = ? ,pjob_phone = ? ,pjob_email = ?,pjob_status = ?,pjob_description = ? WHERE pjob_id = ?";
					$stmt = $pdo->prepare($query);
					$result = $stmt->execute(array($pjob_name,$pjob_clientname,$pjob_organization,$pjob_phone,$pjob_email,$pjob_status,$pjob_description,$pjob_id));
					if(!$result){
						$status = "error";
						$message = "Unable to update";
					}
					else{
						$status = "success";
						$message = "Job successfully updated";
					}
						
				}
			}
		}
		else if($action == "delete"){
			$query = "DELETE FROM ptm_pendingjobs WHERE pjob_id = ?";
			$stmt = $pdo->prepare($query);
			$result = $stmt->execute(array($pjob_id));
			
			if(!$result || $stmt->rowCount() == 0){
				$status = "error";
				$message = "Unable to delete from DB";
			}
			else{
				 $status = "success";
				$message = "Job Succesfully deleted";
			}
			
		}
		
		else{
			$status = "error";
			$message = "Improper action  parameters passed";
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	require_once 'json_encode.php';
?>	