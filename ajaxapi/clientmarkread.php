<?php 
	session_start();
	$status = "";
	$message = "";
	function dead(){
		include 'json_encode.php';
		die();
	}
	if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH'])
			AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
		$status = "error";
		$message = "Only AJAX Requests will be processed";
		dead();
	}
	else if(!(isset($_SESSION['ptm_userid']) && isset($_SESSION['ptm_username']) && isset($_SESSION['ptm_logged']) && isset($_SESSION['ptm_logintype']) )){
		$status = "unauthenticated";
		$message = "Please login first";
		dead();
	}
	else{
		include '../connect.php';
		$userid = $_SESSION['ptm_userid'];
		$query = "UPDATE ptm_clientnotifications SET notification_status = 'read' WHERE notification_clientid = $userid";
		$result = mysql_query($query);
		if(!$result){
			$status = "error";
			$message = "Unable to change notification status";
		}
		else{
			$status = "success";
		}
		include 'json_encode.php';
	}
?>