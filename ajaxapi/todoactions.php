<?php
	session_start();
	require 'authentication_ajax_api.php';
	require '../connect.php';
	
	
	if(isset($_POST['action'])){
		$action = $_POST['action'];
		$userid = $_SESSION['ptm_userid'];
		
		if($action == "add"){
// 			error_log("add",0);
			if(isset($_POST['task_name']) && isset($_POST['admin_id'])){
				$taskname = $_POST['task_name'];
				$adminid = $_POST['admin_id'];
				$stmt = $pdo->prepare("BEGIN");
				$stmt->execute();
// 				error_log("Got params",0);
				$query = "INSERT INTO ptm_todolist (todo_item,todo_adminid) VALUES (?,?)";
				$stmt = $pdo->prepare($query);
				$result = $stmt->execute(array($taskname,$adminid));
				if(!$result || $stmt->rowCount() == 0){
					
					$status = "error";
					$message = "Unable to add item";
				}
				else{
					if($adminid != $userid){
						$query = "INSERT INTO ptm_adminnotifications ( notification_type,notification_adminid,notification_forid) VALUES (?,?,?)";
						$stmt = $pdo->prepare($query);
						$result = $stmt->execute(array("todoupdate",$userid,$adminid));
						if(!$result || $stmt->rowCount() == 0){
							$stmt = $pdo->prepare("ROLLBACK");
							$stmt->execute();
							$status = "error";
							$message = "Unable to add to do list";
						}
						else{
							$stmt = $pdo->prepare("COMMIT");
							$stmt->execute();
						}
						
					}
					else{
						$stmt = $pdo->prepare("COMMIT");
						$stmt->execute();
						$status = "success";
						$message = "Item successfully added";
					}
					$query = "SELECT  todo_id FROM ptm_todolist WHERE todo_item = ? AND todo_adminid = ?";
					$stmt = $pdo->prepare($query);
					$result = $stmt->execute(array($taskname,$adminid));
					if(!$result || $stmt->rowCount() == 0){
						$status = "reload";
					}
					else{
						$status = "success";
						$temp = $stmt->fetch(PDO::FETCH_ASSOC);
						$message = $temp['todo_id'];
					}
										
				}
			}
			else{
				$status = "error";
				$message = "Improper parameters passed";
			}
		}
		
		else if($action == "changestatus"){
			if(isset($_POST['task_id']) && isset($_POST['status'])){
				$taskid = $_POST['task_id'];
				$taskstatus = $_POST['status'];
				
				if($taskstatus != "markdone" && $taskstatus != "markundone"){
					$status = "error";
					$message = "Improper task status provided";
					
				}
				else{
					if($taskstatus == "markdone"){
						$update = "complete";
					}
					else{
						$update = "incomplete";
					}
					$query = "SELECT todo_item FROM ptm_todolist WHERE todo_id = ? && todo_adminid = ?";
					$stmt = $pdo->prepare($query);
					$result = $stmt->execute(array($taskid,$userid));
					if(!$result || $stmt->rowCount() == 0){
						$status = "error";
						$message = "To Do Item not found";
					}
					else{
						$query = "UPDATE ptm_todolist SET todo_status = ? WHERE todo_id = ?";
						$stmt = $pdo->prepare($query);
						$stmt->execute(array($update,$taskid));
						$status = "success";
					}
					
				}
				
			}
			else{
				$status = "error";
				$message = "No task id provided";
			}
		}
		
		
		else if($action == "delete"){
			if(isset($_POST['task_id'])){
				$taskid = $_POST['task_id'];
				$query = "SELECT todo_item FROM ptm_todolist WHERE todo_id = ? && todo_adminid = ?";
				$stmt = $pdo->prepare($query);
				$result = $stmt->execute(array($taskid,$userid));
				if(!$result || $stmt->rowCount() == 0){
					$status = "error";
					$message = "To Do Item not found";
				}
				else{
					$query = "DELETE FROM ptm_todolist WHERE todo_id = ?";
					$stmt = $pdo->prepare($query);
					$result = $stmt->execute(array($taskid));
					if(!$result || $stmt->rowCount() == 0){
						$status = "error";
						$message = "Unable to delete to do item";
					}
					else{
						$status = "success";
						$message = "To do item successfully deleted";
					}
				}
			}
			else{
				$status = "error";
				$message = "Id not provided";
			}
		}
		else{
			$status = "error";
			$message = "Improper Action parameter passed";
		}
		
	} 
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	require 'json_encode.php';
?>