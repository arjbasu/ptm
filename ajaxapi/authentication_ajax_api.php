<?php
$status = "";
$message = "";
function dead(){
	global $status;
	global $message;
	include 'json_encode.php';
	die();
}
if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH'])
		AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
	$status = "error";
	$message = "Only AJAX Requests will be processed";
	dead();
} 
else if(!(isset($_SESSION['ptm_userid']) && isset($_SESSION['ptm_username']) && isset($_SESSION['ptm_logged']) && isset($_SESSION['ptm_logintype']) )){
	$status = "unauthenticated";
	$message = "Please login first";
	dead();
}
else if($_SESSION['ptm_logintype'] != "admin"){
	$status = "error";
	$message = "You do not have appropriate privileges for this action";
	dead();
}

?>