<?php
	session_start();
	include 'connect.php';
	include 'check_authorization.php';
	include 'twiginit.php';
	include 'generatenavdata.php';
	function showclients(){
		global $data;
		global $twig;
		global $pdo;
		$data['name'] = $_SESSION['ptm_username'];
		$query = "SELECT client_name,client_username,client_id,client_organization FROM ptm_client";
		$result = mysql_query($query);
		if(!$result){
			die("Unable to connect to database");
		}
		else{
			$clients = array();
			while($temp = mysql_fetch_assoc($result)){
				array_push($clients, $temp);
			}
			$data['clients'] = $clients;
			echo $twig->render("clients.twig",$data);
		}
		
	}
	if($_SESSION['ptm_logintype'] == "admin"){
		showclients();
	} 
	
	else{
		header("Location:index.php");
	}
?>