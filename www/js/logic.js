		var link = "http://ptmfood.com/portal/";
		var checkobj;
		var loginPage;
		var initialized = false;
		function processLogout(){
        	$.mobile.loading('show');
        	var appid = window.localStorage.getItem("appId");
			var userid = window.localStorage.getItem("userId");
			var logintype = window.localStorage.getItem("loginType");
        	data = {"app_id":appid,"user_id":userid,"login_type":logintype};
        	$.ajax({
        		url:link+"app/applogout.php",
        		type:"POST",
        		data:data,
        		dataType:"JSON",
        		success:function(data){
        			if(data.status == "success"){
        				$.mobile.loading('hide');
        				window.localStorage.clear();
        				$("#loginform").show();
        				$.mobile.changePage("#login",{changeHash:false});
        			}
        			else{
        				$.mobile.loading('hide');
	    				alert(data.message);
        			}
        		},
        		error:function(){
        			$.mobile.loading('hide')
	    			alert("status=" + text + ",error=" + errorT);
        		}
        	});
    	}
		
		function processLogin(){
				$.mobile.loading('show');
		    	var data = $(this).serialize();
		    	console.log(data);	    	
		    	$.mobile.loading('show');
		    	$.ajax({
		    		url:link+"app/applogin.php",
		    		data:data,
		    		type:"POST",
		    		dataType:"json",
		    		success:function(data){
		    			if(data.status == "success"){
		    				$.mobile.loading('hide');
		    				window.localStorage.setItem("appId",data.key);
		    				window.localStorage.setItem("userId",data.userid);
		    				window.localStorage.setItem("loginType",data.logintype);
		    				window.localStorage.setItem("userName",data.fullname);
		    				$("#loginform").hide().trigger("reset");
		    				
		    				if(data.logintype == "admin"){
		    					$.mobile.changePage("#dashboard",{changeHash:false});
		    				}
		    				else{
		    					$.mobile.changePage("#client-dashboard",{changeHash:false});
		    				}
		    				
		    				alert(data.key);
		    			}
		    			else{
		    				$.mobile.loading('hide');
		    				alert(data.message);
		    			}
		    		},
		    		error:function(data,text,errorT){
		    			$.mobile.loading('hide')
		    			alert("status=" + text + ",error=" + errorT);
		    		}
		    	});
	    	return false;
	    	event.preventDefault();
	        }    
			    	
			$(document).on("pagebeforechange",function(event,data){
				console.log("pagebeforechange");
				console.log(initialized);
				console.log(data);
				// console.log(data);
				// checkobj = data.toPage;
				if(initialized === true){
					id = data.toPage[0].id
					if(id === loginPage.id){
						console.log('tologin');
						$("#logout").off("click",processLogout);
						$("#client-logout").off("click",processLogout);
					}
					else if(id === dashboardPage.id){
						console.log('todash');
						$("#loginform").off("submit",processLogin);
					}
					
					else if(id === clientDashboard.id){
						console.log('toclientdash');
						$("#loginform").off("submit",processLogin);
					}
				}
				
			}).on("pagechange",function(event,data){
				// alert("pagechange");
				console.log("pagechange");
				console.log(initialized);
				console.log(data);
				if(initialized === true){
					id = data.toPage[0].id
					if(id === loginPage.id){
						console.log('tologin');
						$("#loginform").on("submit",processLogin).show();
					}
					else if(id === dashboardPage.id){
						console.log('todash');
						console.log(data.toPage);
						$("#logout").on("click",processLogout);
					}
					else if(id === clientDashboard.id){
						console.log('todash');
						console.log(data.toPage);
						$("#client-logout").on("click",processLogout);
					}
				}
			});
        	$(document).on("pageinit",function(){
        		
        		dom = $(document);
        		loginPage = dom.find("#login")[0];
        		dashboardPage = dom.find("#dashboard")[0];
        		clientDashboard = dom.find("#client-dashboard")[0];
        		initialized = true;
        		console.log("pageinit");
        		$("#todolist").hide();
	    		$.support.cors = true;
    			$.mobile.allowCrossDomainPages = true;
    	
	    		$("#todo").click(function(){
	    			var attr = $(this).attr("data-display");
	    			if(attr == "false"){
	    				$("#todolist").slideDown("200");
	    				$(this).attr("data-display","true");
	    			}
	    			else{
	    				$("#todolist").slideUp("200");
	    				$(this).attr("data-display","false")
	    			}
	    			
	    		});
    		
			
			
			});
			
	        
	        

    // Wait for PhoneGap to load
    //
    document.addEventListener("deviceready", onDeviceReady, false);
    

    // PhoneGap is ready
    //
    function onDeviceReady() {
    	// alert("Device Ready");
    	var value = window.localStorage.getItem("appId");
    	// alert("Value:"+value);
    	if(value === undefined || value === null || value === ""){
    		//$("#loginform").show();
    	}
    	else{
    		var logintype = window.localStorage.getItem("loginType");
    		if(logintype == "client"){
    			$.mobile.changePage("#client-dashboard",{changeHash:false});
    		}
    		else if(logintype == "admin"){
    			$.mobile.changePage("#dashboard",{changeHash:false});
    		}
    		
    	}

    	
	}        


