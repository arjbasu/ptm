-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 22, 2014 at 02:05 PM
-- Server version: 5.5.35
-- PHP Version: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ptmfoods`
--

-- --------------------------------------------------------

--
-- Table structure for table `ptm_admin`
--

CREATE TABLE IF NOT EXISTS `ptm_admin` (
  `admin_id` int(5) NOT NULL AUTO_INCREMENT,
  `admin_username` varchar(60) NOT NULL,
  `admin_passhash` varchar(150) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `admin_salt` varchar(100) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ptm_admin`
--

INSERT INTO `ptm_admin` (`admin_id`, `admin_username`, `admin_passhash`, `admin_name`, `admin_timestamp`, `admin_salt`) VALUES
(1, 'Arj', '$1$foreverd$fQ02iF77f3cc8n1BI7cLn.', 'Arjun Basu', '2013-05-19 18:33:40', ''),
(2, 'ryan', '$1$foreverd$3zSWX/5.nOU1g9v0A.XRA0', 'Ryan J. Dolan', '2013-06-05 19:01:54', '');

-- --------------------------------------------------------

--
-- Table structure for table `ptm_adminexcel`
--

CREATE TABLE IF NOT EXISTS `ptm_adminexcel` (
  `excel_rowid` int(11) NOT NULL AUTO_INCREMENT,
  `excel_jobname` varchar(300) NOT NULL,
  `excel_clientname` varchar(200) NOT NULL,
  `excel_organization` varchar(200) NOT NULL,
  `excel_email` varchar(50) NOT NULL,
  `excel_phone` varchar(30) NOT NULL,
  `excel_date` varchar(30) NOT NULL,
  `excel_status` varchar(50) NOT NULL,
  PRIMARY KEY (`excel_rowid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ptm_adminexcel`
--

INSERT INTO `ptm_adminexcel` (`excel_rowid`, `excel_jobname`, `excel_clientname`, `excel_organization`, `excel_email`, `excel_phone`, `excel_date`, `excel_status`) VALUES
(1, 'Abc', 'sdlfkj', 'lkdsjfl', 'lkjdsflk', 'lkjsldkfjl', 'kjlkj', 'lkjlk'),
(2, 'First Job', 'PTM', 'PTM', 'sdflkj@sdf.com', '3342343242', '04/12', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `ptm_adminlog`
--

CREATE TABLE IF NOT EXISTS `ptm_adminlog` (
  `log_id` int(13) NOT NULL AUTO_INCREMENT,
  `log_adminid` int(3) NOT NULL,
  `log_type` varchar(30) NOT NULL,
  `log_jobid` int(12) NOT NULL,
  `log_taskid` int(13) NOT NULL,
  `log_feedid` int(13) NOT NULL,
  `log_clientid` int(13) NOT NULL,
  `log_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ptm_adminnotifications`
--

CREATE TABLE IF NOT EXISTS `ptm_adminnotifications` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_adminid` int(11) NOT NULL,
  `notification_type` varchar(50) NOT NULL,
  `notification_jobid` int(11) NOT NULL,
  `notification_taskid` int(11) NOT NULL,
  `notification_clientid` int(11) NOT NULL,
  `notification_status` varchar(40) NOT NULL DEFAULT 'unread',
  `notification_forid` int(11) NOT NULL,
  `notification_completion` float(5,2) NOT NULL DEFAULT '0.00',
  `notification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `ptm_adminnotifications`
--

INSERT INTO `ptm_adminnotifications` (`notification_id`, `notification_adminid`, `notification_type`, `notification_jobid`, `notification_taskid`, `notification_clientid`, `notification_status`, `notification_forid`, `notification_completion`, `notification_timestamp`) VALUES
(1, 1, 'taskcomplete', 3, 18, 1, 'read', 2, 95.33, '2013-06-18 17:38:40'),
(2, 1, 'jobcomplete', 6, 68, 1, 'read', 2, 100.00, '2013-06-18 17:49:41'),
(3, 1, 'jobcomplete', 6, 69, 1, 'read', 2, 100.00, '2013-06-18 17:50:40'),
(4, 1, 'todoupdate', 0, 0, 0, 'read', 2, 0.00, '2013-06-22 18:50:56'),
(5, 1, 'todoupdate', 0, 0, 0, 'read', 2, 0.00, '2013-06-22 18:52:18'),
(6, 1, 'taskcomplete', 3, 2, 1, 'unread', 2, 95.33, '2013-06-22 19:18:00'),
(7, 1, 'taskcomplete', 3, 2, 1, 'unread', 2, 95.33, '2013-06-22 19:19:13'),
(8, 1, 'taskcomplete', 3, 71, 1, 'unread', 2, 97.03, '2013-06-22 19:19:41'),
(9, 1, 'jobcomplete', 6, 39, 1, 'unread', 2, 100.00, '2013-07-08 17:53:56');

-- --------------------------------------------------------

--
-- Table structure for table `ptm_appkey`
--

CREATE TABLE IF NOT EXISTS `ptm_appkey` (
  `key_id` int(11) NOT NULL AUTO_INCREMENT,
  `key_value` varchar(100) NOT NULL,
  `key_userid` int(11) NOT NULL,
  `key_type` varchar(30) NOT NULL,
  PRIMARY KEY (`key_id`),
  UNIQUE KEY `key_value` (`key_value`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ptm_appkey`
--

INSERT INTO `ptm_appkey` (`key_id`, `key_value`, `key_userid`, `key_type`) VALUES
(1, '39bdae7b31d5581add3223678d8bccfe', 1, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `ptm_client`
--

CREATE TABLE IF NOT EXISTS `ptm_client` (
  `client_id` int(6) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(100) NOT NULL,
  `client_organization` varchar(200) NOT NULL,
  `client_username` varchar(60) NOT NULL,
  `client_passhash` varchar(150) NOT NULL,
  `client_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_salt` varchar(100) NOT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `ptm_client`
--

INSERT INTO `ptm_client` (`client_id`, `client_name`, `client_organization`, `client_username`, `client_passhash`, `client_timestamp`, `client_salt`) VALUES
(1, 'Arjun Basu', 'PTM Foods', 'Arj', '$1$foreverd$nhr3zz3A47E0Wd87YGFZH0', '2013-05-19 19:19:36', ''),
(5, 'Ryan J. Dolan', 'PTM Foods', 'ryan', '$1$foreverd$p4JYabjZU2Jjgz4cpptGx1', '2013-06-02 13:07:52', ''),
(7, 'Peter', 'PTM FOODs', 'pete', '$1$foreverd$SySVrOJEckH2nR5hqmq2/1', '2013-06-16 15:01:47', '');

-- --------------------------------------------------------

--
-- Table structure for table `ptm_clientfeed`
--

CREATE TABLE IF NOT EXISTS `ptm_clientfeed` (
  `feed_id` int(13) NOT NULL AUTO_INCREMENT,
  `feed_post` varchar(1000) NOT NULL,
  `feed_clientid` int(11) NOT NULL,
  `feed_jobid` int(12) NOT NULL,
  `feed_adminid` int(11) NOT NULL,
  `feed_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`feed_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `ptm_clientfeed`
--

INSERT INTO `ptm_clientfeed` (`feed_id`, `feed_post`, `feed_clientid`, `feed_jobid`, `feed_adminid`, `feed_timestamp`) VALUES
(1, 'hello', 1, 3, 1, '2013-06-03 19:16:00'),
(2, 'this', 1, 3, 1, '2013-06-03 19:17:52'),
(3, 'google', 5, 2, 1, '2013-06-03 19:18:26'),
(4, 'random', 1, 3, 1, '2013-06-03 19:25:38'),
(5, 'random2', 1, 3, 1, '2013-06-07 06:54:49'),
(6, 'feedupdate', 1, 3, 1, '2013-06-11 18:39:34'),
(7, 'feedupdate2', 1, 3, 1, '2013-06-11 18:40:35'),
(8, 'feedupdate3', 1, 3, 1, '2013-06-11 18:47:11'),
(9, 'feedupdate4', 1, 3, 1, '2013-06-11 18:47:32'),
(10, 'hello''s', 1, 3, 2, '2013-06-16 17:36:28'),
(11, 'hello''s', 1, 3, 1, '2013-06-18 16:35:48'),
(12, 'randomly posted update', 1, 3, 1, '2013-06-18 16:36:33'),
(13, 'randomly', 1, 3, 1, '0000-00-00 00:00:00'),
(14, 'rando', 1, 3, 1, '0000-00-00 00:00:00'),
(15, 'hello', 1, 6, 1, '2013-06-18 16:43:50'),
(17, 'hello', 1, 4, 1, '2013-06-18 16:52:32');

-- --------------------------------------------------------

--
-- Table structure for table `ptm_clientnotifications`
--

CREATE TABLE IF NOT EXISTS `ptm_clientnotifications` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_adminid` int(11) NOT NULL,
  `notification_type` varchar(50) NOT NULL,
  `notification_jobid` int(11) NOT NULL,
  `notification_taskid` int(11) NOT NULL,
  `notification_clientid` int(11) NOT NULL,
  `notification_status` varchar(40) NOT NULL DEFAULT 'unread',
  `notification_completion` float(5,2) NOT NULL,
  `notification_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `ptm_clientnotifications`
--

INSERT INTO `ptm_clientnotifications` (`notification_id`, `notification_adminid`, `notification_type`, `notification_jobid`, `notification_taskid`, `notification_clientid`, `notification_status`, `notification_completion`, `notification_timestamp`) VALUES
(1, 1, 'taskcomplete', 3, 18, 1, 'unread', 95.33, '2013-06-18 17:38:40'),
(2, 1, 'jobcomplete', 6, 68, 1, 'unread', 100.00, '2013-06-18 17:49:41'),
(3, 1, 'jobcomplete', 6, 69, 1, 'unread', 100.00, '2013-06-18 17:50:40'),
(4, 1, 'taskcomplete', 3, 2, 1, 'unread', 95.33, '2013-06-22 19:18:00'),
(5, 1, 'taskcomplete', 3, 2, 1, 'unread', 95.33, '2013-06-22 19:19:13'),
(6, 1, 'taskcomplete', 3, 71, 1, 'unread', 97.03, '2013-06-22 19:19:41'),
(7, 1, 'jobcomplete', 6, 39, 1, 'unread', 100.00, '2013-07-08 17:53:57');

-- --------------------------------------------------------

--
-- Table structure for table `ptm_joblog`
--

CREATE TABLE IF NOT EXISTS `ptm_joblog` (
  `joblog_id` int(13) NOT NULL AUTO_INCREMENT,
  `joblog_jobid` int(12) NOT NULL,
  `joblog_taskid` int(13) NOT NULL,
  `joblog_adminid` int(3) NOT NULL,
  `joblog_clientid` int(6) NOT NULL,
  `joblog_type` varchar(50) NOT NULL,
  `joblog_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`joblog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ptm_jobs`
--

CREATE TABLE IF NOT EXISTS `ptm_jobs` (
  `job_id` int(20) NOT NULL AUTO_INCREMENT,
  `job_name` varchar(200) NOT NULL,
  `job_description` varchar(1000) NOT NULL,
  `job_clientid` int(6) NOT NULL,
  `job_adminid` int(3) NOT NULL,
  `job_budget` float(12,2) NOT NULL,
  `job_completion` float(5,2) NOT NULL,
  `job_eta` int(5) NOT NULL,
  `job_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `job_paymentstatus` varchar(20) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `ptm_jobs`
--

INSERT INTO `ptm_jobs` (`job_id`, `job_name`, `job_description`, `job_clientid`, `job_adminid`, `job_budget`, `job_completion`, `job_eta`, `job_timestamp`, `job_paymentstatus`) VALUES
(3, 'Tetrapak2', '', 1, 1, 12300.00, 95.33, 0, '2013-06-02 19:08:07', ''),
(4, 'Testing Job Out', '', 1, 2, 4000.00, 100.00, 0, '2013-06-16 17:16:24', ''),
(6, 'New Job ', '', 1, 1, 10000.00, 100.00, 0, '2013-06-16 19:51:31', '');

-- --------------------------------------------------------

--
-- Table structure for table `ptm_paymentlog`
--

CREATE TABLE IF NOT EXISTS `ptm_paymentlog` (
  `paymentlog_id` int(10) NOT NULL AUTO_INCREMENT,
  `paymentlog_clientid` int(12) NOT NULL,
  `paymentlog_jobid` int(13) NOT NULL,
  `paymentlog_amount` double(9,2) NOT NULL,
  `paymentlog_notes` varchar(1000) NOT NULL,
  `paymentlog_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`paymentlog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ptm_pendingjobs`
--

CREATE TABLE IF NOT EXISTS `ptm_pendingjobs` (
  `pjob_id` int(11) NOT NULL AUTO_INCREMENT,
  `pjob_name` varchar(300) NOT NULL,
  `pjob_clientname` varchar(100) NOT NULL,
  `pjob_companyname` varchar(100) NOT NULL,
  `pjob_phone` varchar(20) NOT NULL,
  `pjob_email` varchar(50) NOT NULL,
  `pjob_description` varchar(600) NOT NULL,
  `pjob_status` varchar(60) NOT NULL,
  PRIMARY KEY (`pjob_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ptm_pendingjobs`
--

INSERT INTO `ptm_pendingjobs` (`pjob_id`, `pjob_name`, `pjob_clientname`, `pjob_companyname`, `pjob_phone`, `pjob_email`, `pjob_description`, `pjob_status`) VALUES
(1, 'aalbal', 'sdfs', 'sdfsd', 'sdfsdf', 'sdfsdf', 'sdfsdfs', 'pending'),
(2, 'kjklj', 'kljlk', 'lkjkl', 'lkjl', 'lkj', 'jlkjklj', 'call');

-- --------------------------------------------------------

--
-- Table structure for table `ptm_revenue`
--

CREATE TABLE IF NOT EXISTS `ptm_revenue` (
  `revenue_id` int(10) NOT NULL AUTO_INCREMENT,
  `revenue_date` date NOT NULL,
  `revenue_amount` double(9,2) NOT NULL,
  `revenue_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`revenue_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `ptm_revenue`
--

INSERT INTO `ptm_revenue` (`revenue_id`, `revenue_date`, `revenue_amount`, `revenue_timestamp`) VALUES
(1, '2013-06-07', 500.00, '2013-06-07 13:16:53'),
(2, '2013-06-04', 1000.00, '2013-06-07 13:19:02'),
(3, '2013-04-24', 1000.00, '2013-06-07 13:37:18'),
(4, '2013-07-18', 4000.00, '2013-06-07 13:58:56'),
(5, '2012-11-06', 4000.00, '2013-06-07 16:31:58'),
(6, '2013-08-09', 2500.00, '2013-06-07 16:32:10'),
(7, '2012-09-05', 3400.00, '2013-06-07 16:36:17'),
(8, '2013-11-15', 4888.00, '2013-06-07 16:47:12'),
(9, '2013-02-12', 2660.00, '2013-06-07 16:49:51'),
(10, '2013-05-09', 3000.00, '2013-06-07 16:52:55');

-- --------------------------------------------------------

--
-- Table structure for table `ptm_stats`
--

CREATE TABLE IF NOT EXISTS `ptm_stats` (
  `working_jobs` int(3) NOT NULL DEFAULT '0',
  `pending_jobs` int(3) NOT NULL DEFAULT '0',
  `completed_jobs` int(3) NOT NULL DEFAULT '0',
  `coffee_cups` int(3) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ptm_stats`
--

INSERT INTO `ptm_stats` (`working_jobs`, `pending_jobs`, `completed_jobs`, `coffee_cups`) VALUES
(55, 40, 12, 49);

-- --------------------------------------------------------

--
-- Table structure for table `ptm_tasks`
--

CREATE TABLE IF NOT EXISTS `ptm_tasks` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(200) NOT NULL,
  `task_jobid` int(10) NOT NULL,
  `task_adminid` int(5) NOT NULL,
  `task_status` varchar(30) NOT NULL DEFAULT 'incomplete',
  `task_eta` int(5) NOT NULL,
  `task_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `ptm_tasks`
--

INSERT INTO `ptm_tasks` (`task_id`, `task_name`, `task_jobid`, `task_adminid`, `task_status`, `task_eta`, `task_timestamp`) VALUES
(2, 'Change pH to suit FDA regulations', 3, 1, 'complete', 15, '2013-06-03 06:32:33'),
(3, 'First Task', 3, 1, 'complete', 10, '2013-06-04 08:51:10'),
(4, 'Task 2 random', 3, 1, 'complete', 5, '2013-06-04 08:51:19'),
(5, 'Randomized', 3, 1, 'complete', 15, '2013-06-04 09:09:59'),
(7, 'testingrandom', 3, 1, 'complete', 12, '2013-06-04 18:49:14'),
(8, 'random12', 3, 1, 'complete', 5, '2013-06-04 18:49:52'),
(9, 'new', 3, 1, 'complete', 10, '2013-06-04 18:58:07'),
(11, '20 task', 3, 1, 'complete', 20, '2013-06-04 19:05:15'),
(16, '5task', 3, 1, 'complete', 5, '2013-06-04 19:11:11'),
(17, '10task', 3, 1, 'incomplete', 10, '2013-06-04 19:11:46'),
(18, 'rand', 3, 1, 'complete', 15, '2013-06-04 19:15:12'),
(19, 'rand12', 3, 1, 'complete', 12, '2013-06-04 19:31:27'),
(20, 'therty', 3, 1, 'complete', 30, '2013-06-04 19:32:08'),
(21, 'fifty', 3, 1, 'complete', 50, '2013-06-04 19:34:02'),
(22, 'First Task', 4, 2, 'complete', 15, '2013-06-16 17:16:35'),
(23, 'Second Task', 4, 2, 'complete', 25, '2013-06-16 17:16:45'),
(24, 'Third Task', 4, 2, 'complete', 100, '2013-06-16 17:16:58'),
(25, 'Fourth', 4, 2, 'complete', 200, '2013-06-16 17:17:04'),
(39, '30task', 6, 1, 'complete', 30, '2013-06-17 19:38:05'),
(49, '40task', 6, 1, 'complete', 40, '2013-06-17 19:59:10'),
(65, '20task', 6, 1, 'complete', 20, '2013-06-17 20:18:19'),
(66, '50task', 6, 1, 'complete', 50, '2013-06-17 20:19:36');

-- --------------------------------------------------------

--
-- Table structure for table `ptm_todolist`
--

CREATE TABLE IF NOT EXISTS `ptm_todolist` (
  `todo_id` int(11) NOT NULL AUTO_INCREMENT,
  `todo_item` varchar(200) NOT NULL,
  `todo_status` varchar(40) NOT NULL DEFAULT 'incomplete',
  `todo_adminid` int(3) NOT NULL,
  `todo_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`todo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `ptm_todolist`
--

INSERT INTO `ptm_todolist` (`todo_id`, `todo_item`, `todo_status`, `todo_adminid`, `todo_timestamp`) VALUES
(6, '15', 'complete', 1, '2013-06-22 18:43:40'),
(8, '18', 'complete', 1, '2013-06-22 18:45:46'),
(15, 'Ryan Do this task by tomorrow', 'complete', 2, '2013-06-22 18:52:18');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
