<?php
	session_start();
	include 'check_authorization.php';
	include 'twiginit.php';
	include 'connect.php';
	
	function renderadmin(){
		global $pdo;
		global $data;
		global $twig;
		include 'generatenavdata.php';
		$data['name'] = $_SESSION['ptm_username'];
		$temp  = explode(" ",$data['name']);
		$data['shortname'] = $temp[0];
		$query = "SELECT revenue_date,revenue_amount FROM ptm_revenue ORDER BY revenue_date";
		$result = mysql_query($query);
		$revenue = array();
		if(!$result){
			die("unable to query");
		}
		else{
			while($temp = mysql_fetch_assoc($result)){
				$date = explode("-", $temp['revenue_date']);
				$tempar = array();
				$tempar['day'] = $date[2];
				$tempar['year'] = $date[0];
				$tempar['month'] = $date[1];
				$tempar['revenue'] = $temp['revenue_amount'];
				array_push($revenue,$tempar);
			}
			$data['revenuelist'] = $revenue;
		}
		
		$query = "SELECT * FROM ptm_stats";
		$result = mysql_query($query);
		if(!$result){
			die("Unable to get stats");
		}
		$data['circlestats'] = mysql_fetch_assoc($result);
		
		$query = "SELECT * FROM ptm_adminexcel";
		$result = mysql_query($query);
		$excel = array();
		if(!$result){
			die("Unable to fetch excel entries");
		}
		else{
			while($temp = mysql_fetch_assoc($result)){
				array_push($excel,$temp);
			}
			
		}
		$data['excel'] = $excel;
		echo $twig->render("admin-dashboard.twig",$data);
	}
	function renderclient(){
		global $twig;
		global $data;
		global $pdo;
		include 'generatenavdata.php';
		$data['name'] = $_SESSION['ptm_username'];
		$temp  = explode(" ",$data['name']);
		$data['shortname'] = $temp[0];
		$userid = $_SESSION['ptm_userid'];
		
		$query = "SELECT task_id,task_name,task_jobid,task_status,task_eta,job_clientid FROM ptm_tasks
		INNER JOIN ptm_jobs ON task_jobid = job_id AND job_clientid = '$userid'";
		$result = mysql_query($query);
		if(!$result){
			die("Unable to query");
		}
		$tasks = array();
		while($temp = mysql_fetch_assoc($result)){
			$index = $temp['task_jobid'];
			if(!isset($tasks[$index])){
				$tasks[$index] = array();
			}
			array_push($tasks[$index],$temp);
		}
		
		$query = "SELECT feed_post,admin_name,feed_jobid,DATE_FORMAT(feed_timestamp,'%b %D, %Y') AS feed_timestamp FROM ptm_clientfeed INNER JOIN ptm_admin
		ON feed_adminid = admin_id AND feed_clientid = '$userid'";
		$feed = array();
		$result = mysql_query($query);
		if(!$result){
			die("unable to get feeds");
		}
		else{
			while($temp = mysql_fetch_assoc($result)){
				$temp['feed_post'] = stripslashes($temp['feed_post']);
				$jobid = $temp['feed_jobid'];
				$temp['feed_date'] = $temp['feed_timestamp'];
				
				if(!isset($feed[$jobid])){
					$feed[$jobid] = array();
				}
					
				array_push($feed[$jobid], $temp);
				
			}
		}
		
		
		
		
		
		$query = "SELECT job_id,job_name,job_completion,FORMAT(job_budget,2) AS job_budget,SUM(task_eta),DATE_FORMAT(adddate(job_timestamp,INTERVAL SUM(task_eta) day ),'%M %D, %Y') AS eta FROM ptm_jobs LEFT JOIN ptm_tasks ON
		task_jobid = job_id WHERE job_clientid = $userid GROUP BY job_id ORDER BY job_timestamp DESC";
		$result = mysql_query($query);
		if(!$result){
			die("unable to fetch results");
		}
		$jobs = array();
		
		while($temp = mysql_fetch_assoc($result)){
			$jobid = $temp['job_id'];
			if(isset($tasks[$jobid])){
 				$temp['tasks'] = $tasks[$jobid];
			}
			
			if(isset($feed[$jobid])){
				$temp['feed'] = $feed[$jobid];
			}
			array_push($jobs,$temp);
		}
		
		$data['jobs'] = $jobs;
		//print_r($data);
		echo $twig->render("client-dashboard.twig",$data);
	}
	if($_SESSION['ptm_logintype'] == "admin"){
		renderadmin();
	} 
	else if($_SESSION['ptm_logintype'] == "client"){
		renderclient();
	}
	else{
		header("Location:index.php");
	}
?>