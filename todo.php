<?php
	session_start();
	require_once'check_authorization.php';
	require_once'twiginit.php';
	require_once'connect.php';
	
	function render_todo($adminid){
		global $pdo;
		global $data;
		global $twig;
		include 'generatenavdata.php';
		$data['name'] = $_SESSION['ptm_username'];
		$temp  = explode(" ",$data['name']);
		$data['shortname'] = $temp[0];
		$mine = false;
		if(!$adminid){
			$adminid = $_SESSION['ptm_userid'];
			$mine = true;
		}
		if($mine){
			$data['title'] = "My To-do list";
		}
		else{
			$query = "SELECT admin_name FROM ptm_admin WHERE admin_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($adminid));
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			$data['title'] = $temp['admin_name']."'s To-do list";
			
		}
		$data['mine'] = $mine;
		
		$data['admin_id'] = $adminid;
		
		$query = "SELECT * FROM ptm_todolist WHERE todo_adminid = ?";
		$stmt = $pdo->prepare($query);
		$result = $stmt->execute(array($adminid));
		if(!$result){
			die("Unable to get todo list");
		}
		else{
			$items = array();
			while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
				array_push($items, $temp);
			}
			$data['items'] = $items;
		}
		echo $twig->render("todo.twig",$data);
		
	}
	
	if($_SESSION['ptm_logintype'] == "admin"){
		if(isset($_GET['id'])){
			$adminid = $_GET['id'];
		}
		else{
			$adminid = 0;
		}
		render_todo($adminid);
	}
	
	
?>