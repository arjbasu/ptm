<?php
	session_start();
	include 'check_authorization.php';
	include 'connect.php';
	include 'twiginit.php';
	include 'generatenavdata.php';
	function adminsettings(){
		global $data;
		global $pdo;
		global $twig;
		$clientid = $_SESSION['ptm_userid'];
		$query = "SELECT admin_name,admin_username FROM ptm_admin WHERE admin_id = $clientid";
		$result = mysql_query($query);
		if(!$result){
			die("unable to fetch data");
		}
		else{
			$temp = mysql_fetch_assoc($result);
			$data['name'] = $_SESSION['ptm_username'];
			$data['username'] = $temp['admin_username'];
			$data['fullname'] = $temp['admin_name'];
		}
		
		
		$query = "SELECT * FROM ptm_stats";
		$result = mysql_query($query);
		if(!$result){
			die("Unable to get stats");
		}
		$data['circlestats'] = mysql_fetch_assoc($result);
		
		$query = "SELECT revenue_date, revenue_amount FROM ptm_revenue ORDER BY revenue_date";
		$result = mysql_query($query);
		$revenue = array();
		if(!$result){
			die("unable to fetch revenue");
		}
		else{
			$total = 0;
			while($temp = mysql_fetch_assoc($result)){
				array_push($revenue,$temp);
				$total+= $temp['revenue_amount'];
			}
			$data['revenue'] = $revenue;
			$data['totalrevenue'] = $total;
		}
		echo $twig->render("settings.twig",$data);
	}
	function clientsettings(){
		
	}
	if($_SESSION['ptm_logintype'] == "admin"){
		adminsettings();
	}
	else if($_SESSION['ptm_logintype'] == "client"){
		clientsettings();
	}
	 
?>