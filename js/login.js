$(document).ready(function(){
	$("#loginform").submit(function(){
		console.log("submitted");
		if($("#username").val() === "" || $("#password").val() == ""|| $("#logintype").val() == "NULL"){
			showerror("Please complete all fields before submitting");
		}
		else{
			var data = $(this).serialize();
			console.log(data);
			$.ajax({
				url:"ajaxapi/login.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						showsuccess("Login Successful. One moment....");
						setTimeout(function(){
							window.location = "dashboard.php";
						},"1000")
					}
					else{
						showerror(data.message);
					}
				},
				error:function(){
					showerror("An error occurred");
				}
			})
		}
			
		return false;
	});
});