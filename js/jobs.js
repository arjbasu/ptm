completebtn = '<button class = "btn btn-success disabled showcomplete">' +
'<i class = "icon-ok icon-white"></i>'+
' Complete'+
'</button>';

$(document).ready(function(){
	
//	$(".jobs").on("mouseover",".task",function(){
//		$(this).find(".actions").css("visibility","visible");
//	})
//	.on("mouseout",".task",function(){
//		$(this).find(".actions").css("visibility","hidden");
//	})
	
	$("#edittaskform").submit(function(){
		var taskname = $("#edittaskname").val();
		var tasketa = $("#edittaskform .edittasketa").val();
		var taskid = $("#edittaskid").val();
		var data = {"task_name":taskname,"task_eta":tasketa,"task_id":taskid,"action":"edit"};
		$.ajax({
			url:"ajaxapi/taskactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					showsuccess("Task edited successfully");
					setTimeout(function(){
						window.location = "";
					},800);
					
				}
				else{
					showerror(data.message);
				}
			},
			error:function(){
				showerror("An error occurred. Please try again later");
			}
		});
		return false;
	});
	
	$(".jobs").on("click",".edittask",function(){
		var parent = $(this).parents(".task");
		var taskid = parent.attr("data-taskid");
		var taskname = parent.find(".taskname").html();
		var tasketa = parent.attr('data-tasketa');
		console.log("Eta: "+tasketa);
		$("#edittaskname").val(taskname);
		$("#edittaskform .edittasketa").val(tasketa);
		$("#edittaskid").val(taskid);
		$("#edittaskmodal").modal("show");
		
		return false;
	});
	
	$(".jobs").on("click",".deletetask",function(){
		var r = confirm("Are you sure you want to delete this task? The action is irreversible!");
		if(r === true){
			var parent = $(this).parents(".task");
			var taskid = parent.attr("data-taskid");
			var data = {"task_id":taskid,"action":"delete"};
			$.ajax({
				url:"ajaxapi/taskactions.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						showsuccess("Task successfully deleted");
						completion = data.message;
						parent.parents(".jobs").find(".jobcompletion").html(completion);
						parent.parents(".jobs").find(".bar").css("width",completion+"%");
						parent.fadeOut("300");
						
						if(data.message == 100){
							console.log("100");
							search = parent.parents(".jobs").find(".showcomplete");
							if(search.length == 0)
								parent.parents(".jobs").find(".box-header .box-text").append(completebtn);
						}
						parent.remove();
					}
					else{
						showerror(data.message);
					}
				},
				error:function(){
					
				}
			});
		}
		return false;
	});
	
	$(".job-edit").click(function(){
		var parent = $(this);
		var jobid = parent.parents(".jobs").attr("data-jobid");
		var jobname = parent.parents(".jobs").find(".jobname").html();
		var budget = parent.parents(".jobs").find(".budget").html();
		$("#editjobid").val(jobid);
		$("#editjobname").val(jobname);
		$(".editinputbudget").val(budget);
		$("#editjobmodal").modal("show");
	});
	
	$("#editjobform").submit(function(){
		if($("#editjobname").val() == "" || $("#editjobid").val() ==  "" || $("#editjobform .editinputbudget").val() == ""){
			showerror("Please complete all fields before submitting");
		}
		else{
			var data = $(this).serialize();
			$.ajax({
				url:"ajaxapi/jobactions.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						showsuccess("Job updated successfully");
						setTimeout(function(){
							window.location = "";
						},800);
					}
					else{
						showerror(data.message);
					}
				},
				error:function(){
					showerror("An error occurred");
				}
			});
			
		}
		return false;
	});
	$("#addjobform").submit(function(){
		if($("#jobname").val() == "" || $("#clientid").val() == "NULL" || $("#addjobform .inputbudget").val() == ""){
			console.log($("#jobname").val());
			console.log($("#clientid").val());
			console.log($(".inputbudget").val());
			showerror("Please complete all fields before submitting");
		}
		else{
			var data = $(this).serialize();
			$.ajax({
				url:"ajaxapi/addjob.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						showsuccess("Job added successfully");
						setTimeout(function(){
							window.location = "";
						},800);
					}
					else{
						showerror(data.message);
					}
				},
				error:function(){
					showerror("An error occurred");
				}
			});
		}
		return false;
	});
	
	$(".feedform .update").on("keyup",function(e){
		if(e.keyCode == 13)
			$(this).parent(".feedform").trigger("submit");
	});
	
	$(".addtaskbutton").click(function(){
		$(this).parent().find(".addtask").slideDown("300");
		$(this).hide();
		return false;
	});
	$(".closetask").click(function(){
		$(this).parents(".addtask").slideUp("300",function(){
			$(this).parent().find(".addtaskbutton").fadeIn("200");
		});
		
		return false;
	});
	
	$(".jobs").on("click",".complete",function(){
		var parent = $(this).parents(".task");
		var taskid = parent.attr("data-taskid");
		var action = "";
		if(this.checked){
			action = "markcomplete";
		}
		else{
			action = "markincomplete";
		}
		var completion = parent.parents(".jobs").find(".jobcompletion").html();
		var data = {"task_id":taskid,"task_action":action,"task_completion":completion};
		console.log(data);
		$.ajax({
			url:"ajaxapi/marktaskcomplete.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					parent.parents(".jobs").find(".jobcompletion").html(data.message).parents(".jobs")
					.find(".progress .bar").css("width",data.message+"%");
					var newclass;
					if(action == "markcomplete"){
						if(data.message == 100){
							console.log("100");
							parent.parents(".jobs").find(".box-header .box-text").append(completebtn);
						}
						newclass = "complete";
						oldclass = "incomplete";
					}
					else{
						
						var search = $(parent).parents(".jobs").find(".showcomplete");
						console.log(search);
						if(search.length > 0){
							search.remove();
						}
						newclass = "incomplete";
						oldclass = "complete";
					}
					if(action == "markcomplete"){
						if(!parent.find(".checker span").hasClass("checked")){
							parent.find(".checker span").addClass("checked");
						}
					}
					else{
						if(parent.find(".checker span").hasClass("checked")){
							parent.find(".checker span").removeClass("checked");
						}
					}
					var returnval = parent.find("."+oldclass).not("input").removeClass(oldclass).addClass(newclass);
					console.log(returnval);
				}
				else{
					showerror(data.message);
					
				}
			},
			error:function(){
				showerror("An error occurred");
			}
		
		});
	});
	
	$(".job-delete").click(function(){
		var confirmation = confirm("Are you sure you want to delete this job.This action is irreversible");
		if(confirmation === true){
			var jobid = $(this).parents(".jobs").attr("data-jobid");
			parent = $(this).parents(".jobs");
			var data = {"job_id":jobid};
			$.ajax({
				url:"ajaxapi/deletejob.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						showsuccess("Job successfully deleted");
						parent.fadeOut("300");
						parent.remove();
					}
					else{
						showerror(data.message);
					}
				},
				error:function(){
					showerror("An error occurred . Please try again later");
				}
			});
		}
		
		
	});
	
	$(".addtask").submit(function(){
		var sentdata = $(this).serialize();
		var eta = $(this).find(".task_eta").val();
		var parent = $(this);
		$.ajax({
			url:"ajaxapi/addtask.php",
			type:"POST",
			data:sentdata,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					showsuccess("Task added successfully");
					var response = data.message;
					response = response.split("|");
					var taskid = response[0];
					var completion = response[1];
					var taskname = response[2]
					var html = "	<span class = 'task' data-taskid = '" + taskid + "' data-tasketa = '" + eta + "'> <div class = 'checker'>" +
									" <span><input type = 'checkbox' class = 'complete' value = 'complete'/></span></div>"+			
									"<span class = 'incomplete'>" +
									" <span class = 'taskname'>" + taskname + "</span>" +
									"<span class = 'actions' style = 'margin-left:10px'>" + 
									"<a class = 'edittask'>" +
									"<i class = 'icon-wrench'></i></a> " +
									"<a class = 'deletetask'>" + 
									"<i class = 'icon-trash'></i></a></span>" +
									"<br/></span>";
					parent.parent().find(".alert").remove();
					parent.parent().find(".taskform").append(html).parents(".jobs").find(".jobcompletion").html(completion)
					.parents(".jobs").find(".bar").css("width",completion+"%");
					parent.trigger("reset").find(".closetask").trigger("click");
					var search = parent.parents(".jobs").find(".showcomplete");
					console.log(search);
					if(search.length > 0){
						search.remove();
					}
					
				}
				else{
					showerror(data.message);
				}
			},
			error:function(){
				showerror("An error occurred");
			}
		});
		return false;
	});
	$(".feedform").submit(function(){
		var post = $(this).find(".update").val();
		post = trimfield(post);
		var parent = $(this);
		if(post != ""){
			var clientid = $(this).parents(".jobs").attr("data-clientid");
			var jobid = $(this).parents(".jobs").attr('data-jobid');
			var data = {"job_id":jobid,"job_post":post,"job_clientid":clientid};
			console.log(data);
			$.ajax({
				url:"ajaxapi/postjobfeed.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						parent.trigger("reset");
						var name = $("#username").text();
						var classname = "dpicon ";
						if(name == "Ryan"){
							classame += "fa-icon-briefcase";
						}
						else if(name == "Pete"){
							classname += "fa-icon-beaker";
						}
						else{
							classname += "fa-icon-user"; 
						}
						var html = "<tr><td class = 'dp'>"+
								" <button class = 'btn disabled'><i class = '" + classname + "'></i></button></td>" + 
								"<td> <span class = 'date pull-right'>" + data.message + "</span>" +
										"<p class = 'name'>" + name + "</p>" +
										"<p class = 'post'>" + post + "</p>"
										"</td></tr>";
						var search = parent.parents(".feedwell").find(".alert");
						if(search.length > 0){
							search.remove();
						}
						parent.parents(".feedwell").find(" .feedtable").append(html);
					}
					else{
						showerror(data.message);
					}
				},
				error:function(){
					showerror("An error occurred");
				}
			});
		}
		
		return false;
	});
});