$(document).ready(function(){
	$("#showform").click(function(){
		$(this).fadeOut("200",function(){
			$("#addtodo").slideDown("300");
		});
	});
	
	$(".todo-list").on("click",".todo-delete",function(){
		var todoid = $(this).parents(".todo").attr("data-todoid");
		var parent = $(this);
		var data = {"task_id":todoid,"action":"delete"};
		$.ajax({
			url:"ajaxapi/todoactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					
					parent.parents("li").fadeOut("300",function(){
						$(this).remove();
					});
				}
				else{
					showerror(data.message);
				}
			},
			error:function(){
				showerror("An error occurred! Please try again later");
			}
		});
		return false;
	}).on("click",".markread",function(){
		var todoid = $(this).parents(".todo").attr("data-todoid");
		var parent = $(this);
		var data = {"task_id":todoid,"action":"changestatus","status":"markdone"};
		$.ajax({
			url:"ajaxapi/todoactions.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					parent.parents("li").find(".incomplete").removeClass("incomplete").addClass("complete");
					parent.fadeOut("300",function(){
						$(this).remove();
					});
				}
				else{
					showerror(data.message);
				}
			},
			error:function(){
				showerror("An error occurred! Please try again later");
			}
		});
		return false;
	}); 
	
	
	$("#addtodo").submit(function(){
		parent = $(this);
		if($("#taskname").val() == ""){
			showerror("Please add a taskname ");
		}
		else{
			var name = $("#taskname").val();
			var data = $(this).serialize();
			$.ajax({
				url:"ajaxapi/todoactions.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "reload"){
						window.location = "";
					}
					else if(data.status == "success"){
						if($(".alert").length > 0){
							window.location = "";
							$(".alert").remove();
						}
						showsuccess("Task successfully added");
						$("#closeform").trigger("click");
						parent.trigger("reset");
						var todoid = data.message;
						var todoactions = "";
						if($(".todo-actions").length > 0){
							todoactions = "<span class = 'todo-actions'><a href = '#' class = 'markread'><i class = 'icon-ok'></i></a>" + 
							"<a href = '#' class = 'todo-delete'><i class = 'icon-remove'></i></a></span";
						}
						var html = "<li class = 'todo' data-todoid = '" + todoid + "'><span class = 'incomplete'> " + name + "</span>" 
						+ todoactions + "</li>";
						
						$(".todo-list").append(html);
					}
				},
				error:function(){
					showerror("An error occurred. Please try again later");
				}
			});
		}
		return false;
	});
	
	$("#closeform").click(function(){
		$("#addtodo").slideUp("300",function(){
			$("#showform").fadeIn("200");
		});
		return false;
	});
});

