$(document).ready(function(){
	
	$("#passwordchangemodal").on("hidden",function(){
		$("#passwordchangeform").trigger("reset");
	});
	
	$("#statsform").submit(function(){
		var data = $(this).serialize();
		$.ajax({
			url:"ajaxapi/updatestats.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					showsuccess("Stats successfully updated");
					$("#statsform input").each(function(){
						$(this).attr("value",$(this).val());
					});
				}
				else{
					showerror(data.message);
					$("#statsform").trigger("reset");
				}
				
			},
			error:function(){
				showerror("An error occurred. Please try again later");
				$("#statsform").trigger("reset");
			}
		});
		return false;
	});
	
	$("#passwordchangeform").submit(function(){
		console.log("fire");
		if($("#oldpassword").val() == "" || $("#newpassword").val() == "" || $("#confirmpassword").val() == ""){
			showerror("Please complete all fields before submitting");
		}
		else if($("#newpassword").val() !=  $("#confirmpassword").val()){
			showerror("Passwords do not match");
		}
		else{
			var data = $(this).serialize();
			$.ajax({
				url:"ajaxapi/updatepassword.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						showsuccess("Password successfully updated");
						$("#passwordchangemodal").modal("hide");
					}
					else{
						showerror(data.message);
					}
				},
				error:function(){
					showerror("An error occurred");
				}
			});
		}
		return false;
	});
	
	$("#togglerevenueform").click(function(){
		$(this).hide();
		$("#revenueform").slideDown("300");
	});
	$("#closerevenueform").click(function(){
		$("#revenueform").slideUp("300",function(){
			$("#togglerevenueform").fadeIn("200");
		});
		return false;
	});
});