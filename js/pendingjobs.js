$(document).ready(function(){
	$("#pendingjobsform").submit(function(){
		console.log("hello");
		var data = $(this).serialize();
		console.log(data);
		$.ajax({
			url:"ajaxapi/pendingjobaction.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "success"){
					showsuccess(data.message);
					setTimeout(function(){
						window.location = "";
					},800);
				}
				else{
					showerror(data.message);
				}
			},
			error:function(){
				showerror("An error occurred. Please try again later");
			}
		});
		return false;
	});
	
	$(".job-edit").click(function(){
		var parent = $(this);
		var jobid = $(this).parents(".jobs").attr("data-jobid");
		var element = parent.parents(".jobs");
		var form = $("#pendingjobsform");
		form.find(":input[name = 'pjob_email']").val(element.find(".email").text());
		form.find(":input[name = 'pjob_name']").val(element.find(".jobname").text());
		form.find(":input[name = 'pjob_description']").val(element.find(".description").text());
		form.find(":input[name = 'pjob_phone']").val(element.find(".phone").text());
		form.find(":input[name = 'pjob_status']").val(element.find(".pjob_status").text());
		console.log("Text:"+element.find(".pjob_status").text());
		form.find(":input[name = 'pjob_clientname']").val(element.find(".client_name").text());
		form.find(":input[name = 'pjob_organization']").val(element.find(".company_name").text());
		form.find(":input[name = 'pjob_id']").val(jobid);
		form.find(":input[name = 'action']").val("update");
		$("#addjobmodal").modal("show");
		
	});
	
	$("#addjobmodal").on("hidden",function(){
		$("#pendingjobsform").trigger("reset").find(":input[name = 'action']").val("create");
		
	});
	
	
	
	$(".job-delete").click(function(){
		var action = confirm("Are you sure you want to delete this task?");
		if(action == true){
			var parent = $(this);
			var jobid = $(this).parents(".jobs").attr("data-jobid");
			//console.log("jobid:"+jobid);
			
			var data = {"pjob_id":jobid,"action":"delete"};
			console.log(data);
			$.ajax({
				url:"ajaxapi/pendingjobaction.php",
				type:"POST",
				data:data,
				datatype:"JSON",
				success:function(data){
					console.log(data);
					if(data.status == "success"){
						parent.parents(".jobs").fadeOut("300",function(){
							$(this).remove();
						});
					}
					else{
						showerror(data.message);
					}
				},
				error:function(){
					showerror("An error occurred");
				}
			})
		}
	});
});