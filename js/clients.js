$(document).ready(function(){
	$("#addclientform").submit(function(){
		if($("#name").val() == "" || $("#username") == "" || $("#organization").val() == ""
			||$("#clientpass").val() == "" || $("#confirmpass").val() == ""	){
			showerror("Please complete all fields before submitting");
		}
		else if($("#clientpass").val() != $("#confirmpass").val()){
			showerror("Passwords do not match");
		}
		else{
			var data = $(this).serialize();
			$.ajax({
				url:"ajaxapi/createclient.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						showsuccess("Client Created successfully");
						setTimeout(function(){
							window.location = "";
						},800);
					}
					else{
						showerror(data.message);
					}
				},
				error:function(){
					showerror("An error occurred");
				}
			});
		}
		return false;
	});
	
	$(".delete").click(function(){
		var option = confirm("Are you sure you want to delete this client?");
		if(option === true){
			var clientid = $(this).parents("tr").attr("data-clientid");
			var data = {"client_id":clientid,"action":"delete"};
			$.ajax({
				url:"ajaxapi/editclient.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "success"){
						showsuccess("Client Successfully deleted");
						setTimeout(function(){
							window.location = "";
						},800);
					}
					else{
						showerror(data.message);
					}
				}
			})
		}
		return false;
	});
});